//
//  NotificationModel.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 26/03/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit

class NotificationModel: NSObject {
    
    var id: String
    var title: String
    var message: String
    var insertedDate: String
    
    init(id: String, title: String, message: String, insertedDate: String) {
        self.id = id
        self.title = title
        self.message = message
        self.insertedDate = insertedDate
    }

}
