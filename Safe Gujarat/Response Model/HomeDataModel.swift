//
//  HomeDataModel.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 01/04/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit

class HomeDataModel: NSObject {

}

class HomeSectionModel: NSObject {
    var id: String
    var title: String
    var detail: String
    var image: String
    var isExpand: String
    var rowData: [HomeRowDataModel]
    
    init(id: String, title: String, detail: String, image: String, isExpand: String, rowData: [HomeRowDataModel]) {
        self.id = id
        self.title = title
        self.detail = detail
        self.image = image
        self.isExpand = isExpand
        self.rowData = rowData
    }
}

class HomeRowDataModel: NSObject {
    var id: String
    var title: String
    var detail: String
    var latitude: String
    var longtitide: String
    var forLogin: String
    var victimeId: String
    var age: String
    var image: UIImage
    var isExpand: String
    
    init(id: String, title: String, detail: String, latitude: String, longtitide: String, forLogin: String, victimeId: String, age: String, image: UIImage, isExpand: String) {
        self.id = id
        self.title = title
        self.detail = detail
        self.latitude = latitude
        self.longtitide = longtitide
        self.forLogin = forLogin
        self.victimeId = victimeId
        self.age = age
        self.image = image
        self.isExpand = isExpand
    }
}
