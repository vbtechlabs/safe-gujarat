//
//  UserInfoModel.swift
//
//  Created by VBT Mac on 25/03/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class UserInfoModel: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let contactNo = "ContactNo"
    static let followInstructionQuarantine = "FollowInstructionQuarantine"
    static let placeVisitAfterQuarantine = "PlaceVisitAfterQuarantine"
    static let haveYouContactWithConfirmedCases = "HaveYouContactWithConfirmedCases"
    static let breathingText = "BreathingText"
    static let screenedAtAirport = "ScreenedAtAirport"
    static let coughText = "CoughText"
    static let feveText = "FeveText"
    static let cityName = "CityName"
    static let haveYouBeenExplainedAfterQuarantineText = "HaveYouBeenExplainedAfterQuarantineText"
    static let aNYURTIORNOTText = "ANYURTIORNOTText"
    static let handWashText = "HandWashText"
    static let modifiedDate = "ModifiedDate"
    static let placeVisitAfterQuarantineText = "PlaceVisitAfterQuarantineText"
    static let gender = "Gender"
    static let longitude = "Longitude"
    static let createdDate = "CreatedDate"
    static let cough = "Cough"
    static let riskPersonText = "RiskPersonText"
    static let riskPerson = "RiskPerson"
    static let suppliesKitNeededText = "SuppliesKitNeededText"
    static let medicinesTaken = "MedicinesTaken"
    static let breathingDifficulty = "BreathingDifficulty"
    static let imageData = "Image_Data"
    static let currentAddress = "CurrentAddress"
    static let callsRecievedThreeTimesText = "CallsRecievedThreeTimesText"
    static let aNYURTIORNOT = "ANYURTIORNOT"
    static let permanentAddress = "PermanentAddress"
    static let lastName = "LastName"
    static let stateId = "StateId"
    static let forLogin = "For_Login"
    static let tracebleText = "TracebleText"
    static let quarantineFromDate = "QuarantineFromDate"
    static let anyDoctorConsultedText = "AnyDoctorConsultedText"
    static let quarantineStampOnHandText = "QuarantineStampOnHandText"
    static let screenedAtAirportText = "ScreenedAtAirportText"
    static let policeStaionName = "PoliceStaionName"
    static let createdUserId = "CreatedUserId"
    static let quarantineToDate = "QuarantineToDate"
    static let goesToWorkText = "GoesToWorkText"
    static let firstName = "FirstName"
    static let goesToWork = "GoesToWork"
    static let handWash = "HandWash"
    static let maintainDistanceMeter = "MaintainDistanceMeter"
    static let policeStationId = "PoliceStationId"
    static let alternatContactNo = "AlternatContactNo"
    static let followInstructionQuarantineText = "FollowInstructionQuarantineText"
    static let modifiedUserId = "ModifiedUserId"
    static let goesOutOfHouse = "GoesOutOfHouse"
    static let isActive = "IsActive"
    static let kitRecievedText = "KitRecievedText"
    static let genderText = "GenderText"
    static let listOfVisitedCountryIn14Days = "ListOfVisitedCountryIn14Days"
    static let fumigationDoneText = "FumigationDoneText"
    static let weakness = "Weakness"
    static let age = "Age"
    static let medicinesTakenText = "MedicinesTakenText"
    static let cityId = "CityId"
    static let goesOutOfHouseText = "GoesOutOfHouseText"
    static let stateName = "StateName"
    static let victimeId = "VictimeId"
    static let quarantineID = "QuarantineID"
    static let fumigationDone = "FumigationDone"
    static let quarantineStampOnHand = "QuarantineStampOnHand"
    static let latitude = "Latitude"
    static let traceble = "Traceble"
    static let haveYouContactWithConfirmedCasesText = "HaveYouContactWithConfirmedCasesText"
    static let haveYouBeenExplainedAfterQuarantine = "HaveYouBeenExplainedAfterQuarantine"
    static let suppliesKitNeeded = "SuppliesKitNeeded"
    static let anyDoctorConsulted = "AnyDoctorConsulted"
    static let lunchDinerNeededText = "Lunch_DinerNeededText"
    static let middleName = "MiddleName"
    static let wearsMask = "WearsMask"
    static let wearsMaskText = "WearsMaskText"
    static let kitRecieved = "KitRecieved"
    static let isDelete = "IsDelete"
    static let lunchDinerNeeded = "Lunch_DinerNeeded"
    static let maintainDistanceMeterText = "MaintainDistanceMeterText"
    static let callsRecievedThreeTimes = "CallsRecievedThreeTimes"
    static let fever = "Fever"
    static let weaknessText = "WeaknessText"
  }

  // MARK: Properties
  public var contactNo: Int?
  public var followInstructionQuarantine: Bool? = false
  public var placeVisitAfterQuarantine: Bool? = false
  public var haveYouContactWithConfirmedCases: Bool? = false
  public var breathingText: String?
  public var screenedAtAirport: Bool? = false
  public var coughText: String?
  public var feveText: String?
  public var cityName: String?
  public var haveYouBeenExplainedAfterQuarantineText: String?
  public var aNYURTIORNOTText: String?
  public var handWashText: String?
  public var modifiedDate: String?
  public var placeVisitAfterQuarantineText: String?
  public var gender: Int?
  public var longitude: Int?
  public var createdDate: String?
  public var cough: Bool? = false
  public var riskPersonText: String?
  public var riskPerson: Bool? = false
  public var suppliesKitNeededText: String?
  public var medicinesTaken: Bool? = false
  public var breathingDifficulty: Bool? = false
  public var imageData: String?
  public var currentAddress: String?
  public var callsRecievedThreeTimesText: String?
  public var aNYURTIORNOT: Bool? = false
  public var permanentAddress: String?
  public var lastName: String?
  public var stateId: Int?
  public var forLogin: Int?
  public var tracebleText: String?
  public var quarantineFromDate: String?
  public var anyDoctorConsultedText: String?
  public var quarantineStampOnHandText: String?
  public var screenedAtAirportText: String?
  public var policeStaionName: String?
  public var createdUserId: Int?
  public var quarantineToDate: String?
  public var goesToWorkText: String?
  public var firstName: String?
  public var goesToWork: Bool? = false
  public var handWash: Bool? = false
  public var maintainDistanceMeter: Bool? = false
  public var policeStationId: Int?
  public var alternatContactNo: Int?
  public var followInstructionQuarantineText: String?
  public var modifiedUserId: Int?
  public var goesOutOfHouse: Bool? = false
  public var isActive: Bool? = false
  public var kitRecievedText: String?
  public var genderText: String?
  public var listOfVisitedCountryIn14Days: String?
  public var fumigationDoneText: String?
  public var weakness: Bool? = false
  public var age: Int?
  public var medicinesTakenText: String?
  public var cityId: Int?
  public var goesOutOfHouseText: String?
  public var stateName: String?
  public var victimeId: Int?
  public var quarantineID: Int?
  public var fumigationDone: Bool? = false
  public var quarantineStampOnHand: Bool? = false
  public var latitude: Int?
  public var traceble: Bool? = false
  public var haveYouContactWithConfirmedCasesText: String?
  public var haveYouBeenExplainedAfterQuarantine: Bool? = false
  public var suppliesKitNeeded: Bool? = false
  public var anyDoctorConsulted: Bool? = false
  public var lunchDinerNeededText: String?
  public var middleName: String?
  public var wearsMask: Bool? = false
  public var wearsMaskText: String?
  public var kitRecieved: Bool? = false
  public var isDelete: Bool? = false
  public var lunchDinerNeeded: Bool? = false
  public var maintainDistanceMeterText: String?
  public var callsRecievedThreeTimes: Bool? = false
  public var fever: Bool? = false
  public var weaknessText: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    contactNo = json[SerializationKeys.contactNo].int
    followInstructionQuarantine = json[SerializationKeys.followInstructionQuarantine].boolValue
    placeVisitAfterQuarantine = json[SerializationKeys.placeVisitAfterQuarantine].boolValue
    haveYouContactWithConfirmedCases = json[SerializationKeys.haveYouContactWithConfirmedCases].boolValue
    breathingText = json[SerializationKeys.breathingText].string
    screenedAtAirport = json[SerializationKeys.screenedAtAirport].boolValue
    coughText = json[SerializationKeys.coughText].string
    feveText = json[SerializationKeys.feveText].string
    cityName = json[SerializationKeys.cityName].string
    haveYouBeenExplainedAfterQuarantineText = json[SerializationKeys.haveYouBeenExplainedAfterQuarantineText].string
    aNYURTIORNOTText = json[SerializationKeys.aNYURTIORNOTText].string
    handWashText = json[SerializationKeys.handWashText].string
    modifiedDate = json[SerializationKeys.modifiedDate].string
    placeVisitAfterQuarantineText = json[SerializationKeys.placeVisitAfterQuarantineText].string
    gender = json[SerializationKeys.gender].int
    longitude = json[SerializationKeys.longitude].int
    createdDate = json[SerializationKeys.createdDate].string
    cough = json[SerializationKeys.cough].boolValue
    riskPersonText = json[SerializationKeys.riskPersonText].string
    riskPerson = json[SerializationKeys.riskPerson].boolValue
    suppliesKitNeededText = json[SerializationKeys.suppliesKitNeededText].string
    medicinesTaken = json[SerializationKeys.medicinesTaken].boolValue
    breathingDifficulty = json[SerializationKeys.breathingDifficulty].boolValue
    imageData = json[SerializationKeys.imageData].string
    currentAddress = json[SerializationKeys.currentAddress].string
    callsRecievedThreeTimesText = json[SerializationKeys.callsRecievedThreeTimesText].string
    aNYURTIORNOT = json[SerializationKeys.aNYURTIORNOT].boolValue
    permanentAddress = json[SerializationKeys.permanentAddress].string
    lastName = json[SerializationKeys.lastName].string
    stateId = json[SerializationKeys.stateId].int
    forLogin = json[SerializationKeys.forLogin].int
    tracebleText = json[SerializationKeys.tracebleText].string
    quarantineFromDate = json[SerializationKeys.quarantineFromDate].string
    anyDoctorConsultedText = json[SerializationKeys.anyDoctorConsultedText].string
    quarantineStampOnHandText = json[SerializationKeys.quarantineStampOnHandText].string
    screenedAtAirportText = json[SerializationKeys.screenedAtAirportText].string
    policeStaionName = json[SerializationKeys.policeStaionName].string
    createdUserId = json[SerializationKeys.createdUserId].int
    quarantineToDate = json[SerializationKeys.quarantineToDate].string
    goesToWorkText = json[SerializationKeys.goesToWorkText].string
    firstName = json[SerializationKeys.firstName].string
    goesToWork = json[SerializationKeys.goesToWork].boolValue
    handWash = json[SerializationKeys.handWash].boolValue
    maintainDistanceMeter = json[SerializationKeys.maintainDistanceMeter].boolValue
    policeStationId = json[SerializationKeys.policeStationId].int
    alternatContactNo = json[SerializationKeys.alternatContactNo].int
    followInstructionQuarantineText = json[SerializationKeys.followInstructionQuarantineText].string
    modifiedUserId = json[SerializationKeys.modifiedUserId].int
    goesOutOfHouse = json[SerializationKeys.goesOutOfHouse].boolValue
    isActive = json[SerializationKeys.isActive].boolValue
    kitRecievedText = json[SerializationKeys.kitRecievedText].string
    genderText = json[SerializationKeys.genderText].string
    listOfVisitedCountryIn14Days = json[SerializationKeys.listOfVisitedCountryIn14Days].string
    fumigationDoneText = json[SerializationKeys.fumigationDoneText].string
    weakness = json[SerializationKeys.weakness].boolValue
    age = json[SerializationKeys.age].int
    medicinesTakenText = json[SerializationKeys.medicinesTakenText].string
    cityId = json[SerializationKeys.cityId].int
    goesOutOfHouseText = json[SerializationKeys.goesOutOfHouseText].string
    stateName = json[SerializationKeys.stateName].string
    victimeId = json[SerializationKeys.victimeId].int
    quarantineID = json[SerializationKeys.quarantineID].int
    fumigationDone = json[SerializationKeys.fumigationDone].boolValue
    quarantineStampOnHand = json[SerializationKeys.quarantineStampOnHand].boolValue
    latitude = json[SerializationKeys.latitude].int
    traceble = json[SerializationKeys.traceble].boolValue
    haveYouContactWithConfirmedCasesText = json[SerializationKeys.haveYouContactWithConfirmedCasesText].string
    haveYouBeenExplainedAfterQuarantine = json[SerializationKeys.haveYouBeenExplainedAfterQuarantine].boolValue
    suppliesKitNeeded = json[SerializationKeys.suppliesKitNeeded].boolValue
    anyDoctorConsulted = json[SerializationKeys.anyDoctorConsulted].boolValue
    lunchDinerNeededText = json[SerializationKeys.lunchDinerNeededText].string
    middleName = json[SerializationKeys.middleName].string
    wearsMask = json[SerializationKeys.wearsMask].boolValue
    wearsMaskText = json[SerializationKeys.wearsMaskText].string
    kitRecieved = json[SerializationKeys.kitRecieved].boolValue
    isDelete = json[SerializationKeys.isDelete].boolValue
    lunchDinerNeeded = json[SerializationKeys.lunchDinerNeeded].boolValue
    maintainDistanceMeterText = json[SerializationKeys.maintainDistanceMeterText].string
    callsRecievedThreeTimes = json[SerializationKeys.callsRecievedThreeTimes].boolValue
    fever = json[SerializationKeys.fever].boolValue
    weaknessText = json[SerializationKeys.weaknessText].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = contactNo { dictionary[SerializationKeys.contactNo] = value }
    dictionary[SerializationKeys.followInstructionQuarantine] = followInstructionQuarantine
    dictionary[SerializationKeys.placeVisitAfterQuarantine] = placeVisitAfterQuarantine
    dictionary[SerializationKeys.haveYouContactWithConfirmedCases] = haveYouContactWithConfirmedCases
    if let value = breathingText { dictionary[SerializationKeys.breathingText] = value }
    dictionary[SerializationKeys.screenedAtAirport] = screenedAtAirport
    if let value = coughText { dictionary[SerializationKeys.coughText] = value }
    if let value = feveText { dictionary[SerializationKeys.feveText] = value }
    if let value = cityName { dictionary[SerializationKeys.cityName] = value }
    if let value = haveYouBeenExplainedAfterQuarantineText { dictionary[SerializationKeys.haveYouBeenExplainedAfterQuarantineText] = value }
    if let value = aNYURTIORNOTText { dictionary[SerializationKeys.aNYURTIORNOTText] = value }
    if let value = handWashText { dictionary[SerializationKeys.handWashText] = value }
    if let value = modifiedDate { dictionary[SerializationKeys.modifiedDate] = value }
    if let value = placeVisitAfterQuarantineText { dictionary[SerializationKeys.placeVisitAfterQuarantineText] = value }
    if let value = gender { dictionary[SerializationKeys.gender] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = createdDate { dictionary[SerializationKeys.createdDate] = value }
    dictionary[SerializationKeys.cough] = cough
    if let value = riskPersonText { dictionary[SerializationKeys.riskPersonText] = value }
    dictionary[SerializationKeys.riskPerson] = riskPerson
    if let value = suppliesKitNeededText { dictionary[SerializationKeys.suppliesKitNeededText] = value }
    dictionary[SerializationKeys.medicinesTaken] = medicinesTaken
    dictionary[SerializationKeys.breathingDifficulty] = breathingDifficulty
    if let value = imageData { dictionary[SerializationKeys.imageData] = value }
    if let value = currentAddress { dictionary[SerializationKeys.currentAddress] = value }
    if let value = callsRecievedThreeTimesText { dictionary[SerializationKeys.callsRecievedThreeTimesText] = value }
    dictionary[SerializationKeys.aNYURTIORNOT] = aNYURTIORNOT
    if let value = permanentAddress { dictionary[SerializationKeys.permanentAddress] = value }
    if let value = lastName { dictionary[SerializationKeys.lastName] = value }
    if let value = stateId { dictionary[SerializationKeys.stateId] = value }
    if let value = forLogin { dictionary[SerializationKeys.forLogin] = value }
    if let value = tracebleText { dictionary[SerializationKeys.tracebleText] = value }
    if let value = quarantineFromDate { dictionary[SerializationKeys.quarantineFromDate] = value }
    if let value = anyDoctorConsultedText { dictionary[SerializationKeys.anyDoctorConsultedText] = value }
    if let value = quarantineStampOnHandText { dictionary[SerializationKeys.quarantineStampOnHandText] = value }
    if let value = screenedAtAirportText { dictionary[SerializationKeys.screenedAtAirportText] = value }
    if let value = policeStaionName { dictionary[SerializationKeys.policeStaionName] = value }
    if let value = createdUserId { dictionary[SerializationKeys.createdUserId] = value }
    if let value = quarantineToDate { dictionary[SerializationKeys.quarantineToDate] = value }
    if let value = goesToWorkText { dictionary[SerializationKeys.goesToWorkText] = value }
    if let value = firstName { dictionary[SerializationKeys.firstName] = value }
    dictionary[SerializationKeys.goesToWork] = goesToWork
    dictionary[SerializationKeys.handWash] = handWash
    dictionary[SerializationKeys.maintainDistanceMeter] = maintainDistanceMeter
    if let value = policeStationId { dictionary[SerializationKeys.policeStationId] = value }
    if let value = alternatContactNo { dictionary[SerializationKeys.alternatContactNo] = value }
    if let value = followInstructionQuarantineText { dictionary[SerializationKeys.followInstructionQuarantineText] = value }
    if let value = modifiedUserId { dictionary[SerializationKeys.modifiedUserId] = value }
    dictionary[SerializationKeys.goesOutOfHouse] = goesOutOfHouse
    dictionary[SerializationKeys.isActive] = isActive
    if let value = kitRecievedText { dictionary[SerializationKeys.kitRecievedText] = value }
    if let value = genderText { dictionary[SerializationKeys.genderText] = value }
    if let value = listOfVisitedCountryIn14Days { dictionary[SerializationKeys.listOfVisitedCountryIn14Days] = value }
    if let value = fumigationDoneText { dictionary[SerializationKeys.fumigationDoneText] = value }
    dictionary[SerializationKeys.weakness] = weakness
    if let value = age { dictionary[SerializationKeys.age] = value }
    if let value = medicinesTakenText { dictionary[SerializationKeys.medicinesTakenText] = value }
    if let value = cityId { dictionary[SerializationKeys.cityId] = value }
    if let value = goesOutOfHouseText { dictionary[SerializationKeys.goesOutOfHouseText] = value }
    if let value = stateName { dictionary[SerializationKeys.stateName] = value }
    if let value = victimeId { dictionary[SerializationKeys.victimeId] = value }
    if let value = quarantineID { dictionary[SerializationKeys.quarantineID] = value }
    dictionary[SerializationKeys.fumigationDone] = fumigationDone
    dictionary[SerializationKeys.quarantineStampOnHand] = quarantineStampOnHand
    if let value = latitude { dictionary[SerializationKeys.latitude] = value }
    dictionary[SerializationKeys.traceble] = traceble
    if let value = haveYouContactWithConfirmedCasesText { dictionary[SerializationKeys.haveYouContactWithConfirmedCasesText] = value }
    dictionary[SerializationKeys.haveYouBeenExplainedAfterQuarantine] = haveYouBeenExplainedAfterQuarantine
    dictionary[SerializationKeys.suppliesKitNeeded] = suppliesKitNeeded
    dictionary[SerializationKeys.anyDoctorConsulted] = anyDoctorConsulted
    if let value = lunchDinerNeededText { dictionary[SerializationKeys.lunchDinerNeededText] = value }
    if let value = middleName { dictionary[SerializationKeys.middleName] = value }
    dictionary[SerializationKeys.wearsMask] = wearsMask
    if let value = wearsMaskText { dictionary[SerializationKeys.wearsMaskText] = value }
    dictionary[SerializationKeys.kitRecieved] = kitRecieved
    dictionary[SerializationKeys.isDelete] = isDelete
    dictionary[SerializationKeys.lunchDinerNeeded] = lunchDinerNeeded
    if let value = maintainDistanceMeterText { dictionary[SerializationKeys.maintainDistanceMeterText] = value }
    dictionary[SerializationKeys.callsRecievedThreeTimes] = callsRecievedThreeTimes
    dictionary[SerializationKeys.fever] = fever
    if let value = weaknessText { dictionary[SerializationKeys.weaknessText] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.contactNo = aDecoder.decodeObject(forKey: SerializationKeys.contactNo) as? Int
    self.followInstructionQuarantine = aDecoder.decodeBool(forKey: SerializationKeys.followInstructionQuarantine)
    self.placeVisitAfterQuarantine = aDecoder.decodeBool(forKey: SerializationKeys.placeVisitAfterQuarantine)
    self.haveYouContactWithConfirmedCases = aDecoder.decodeBool(forKey: SerializationKeys.haveYouContactWithConfirmedCases)
    self.breathingText = aDecoder.decodeObject(forKey: SerializationKeys.breathingText) as? String
    self.screenedAtAirport = aDecoder.decodeBool(forKey: SerializationKeys.screenedAtAirport)
    self.coughText = aDecoder.decodeObject(forKey: SerializationKeys.coughText) as? String
    self.feveText = aDecoder.decodeObject(forKey: SerializationKeys.feveText) as? String
    self.cityName = aDecoder.decodeObject(forKey: SerializationKeys.cityName) as? String
    self.haveYouBeenExplainedAfterQuarantineText = aDecoder.decodeObject(forKey: SerializationKeys.haveYouBeenExplainedAfterQuarantineText) as? String
    self.aNYURTIORNOTText = aDecoder.decodeObject(forKey: SerializationKeys.aNYURTIORNOTText) as? String
    self.handWashText = aDecoder.decodeObject(forKey: SerializationKeys.handWashText) as? String
    self.modifiedDate = aDecoder.decodeObject(forKey: SerializationKeys.modifiedDate) as? String
    self.placeVisitAfterQuarantineText = aDecoder.decodeObject(forKey: SerializationKeys.placeVisitAfterQuarantineText) as? String
    self.gender = aDecoder.decodeObject(forKey: SerializationKeys.gender) as? Int
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? Int
    self.createdDate = aDecoder.decodeObject(forKey: SerializationKeys.createdDate) as? String
    self.cough = aDecoder.decodeBool(forKey: SerializationKeys.cough)
    self.riskPersonText = aDecoder.decodeObject(forKey: SerializationKeys.riskPersonText) as? String
    self.riskPerson = aDecoder.decodeBool(forKey: SerializationKeys.riskPerson)
    self.suppliesKitNeededText = aDecoder.decodeObject(forKey: SerializationKeys.suppliesKitNeededText) as? String
    self.medicinesTaken = aDecoder.decodeBool(forKey: SerializationKeys.medicinesTaken)
    self.breathingDifficulty = aDecoder.decodeBool(forKey: SerializationKeys.breathingDifficulty)
    self.imageData = aDecoder.decodeObject(forKey: SerializationKeys.imageData) as? String
    self.currentAddress = aDecoder.decodeObject(forKey: SerializationKeys.currentAddress) as? String
    self.callsRecievedThreeTimesText = aDecoder.decodeObject(forKey: SerializationKeys.callsRecievedThreeTimesText) as? String
    self.aNYURTIORNOT = aDecoder.decodeBool(forKey: SerializationKeys.aNYURTIORNOT)
    self.permanentAddress = aDecoder.decodeObject(forKey: SerializationKeys.permanentAddress) as? String
    self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
    self.stateId = aDecoder.decodeObject(forKey: SerializationKeys.stateId) as? Int
    self.forLogin = aDecoder.decodeObject(forKey: SerializationKeys.forLogin) as? Int
    self.tracebleText = aDecoder.decodeObject(forKey: SerializationKeys.tracebleText) as? String
    self.quarantineFromDate = aDecoder.decodeObject(forKey: SerializationKeys.quarantineFromDate) as? String
    self.anyDoctorConsultedText = aDecoder.decodeObject(forKey: SerializationKeys.anyDoctorConsultedText) as? String
    self.quarantineStampOnHandText = aDecoder.decodeObject(forKey: SerializationKeys.quarantineStampOnHandText) as? String
    self.screenedAtAirportText = aDecoder.decodeObject(forKey: SerializationKeys.screenedAtAirportText) as? String
    self.policeStaionName = aDecoder.decodeObject(forKey: SerializationKeys.policeStaionName) as? String
    self.createdUserId = aDecoder.decodeObject(forKey: SerializationKeys.createdUserId) as? Int
    self.quarantineToDate = aDecoder.decodeObject(forKey: SerializationKeys.quarantineToDate) as? String
    self.goesToWorkText = aDecoder.decodeObject(forKey: SerializationKeys.goesToWorkText) as? String
    self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
    self.goesToWork = aDecoder.decodeBool(forKey: SerializationKeys.goesToWork)
    self.handWash = aDecoder.decodeBool(forKey: SerializationKeys.handWash)
    self.maintainDistanceMeter = aDecoder.decodeBool(forKey: SerializationKeys.maintainDistanceMeter)
    self.policeStationId = aDecoder.decodeObject(forKey: SerializationKeys.policeStationId) as? Int
    self.alternatContactNo = aDecoder.decodeObject(forKey: SerializationKeys.alternatContactNo) as? Int
    self.followInstructionQuarantineText = aDecoder.decodeObject(forKey: SerializationKeys.followInstructionQuarantineText) as? String
    self.modifiedUserId = aDecoder.decodeObject(forKey: SerializationKeys.modifiedUserId) as? Int
    self.goesOutOfHouse = aDecoder.decodeBool(forKey: SerializationKeys.goesOutOfHouse)
    self.isActive = aDecoder.decodeBool(forKey: SerializationKeys.isActive)
    self.kitRecievedText = aDecoder.decodeObject(forKey: SerializationKeys.kitRecievedText) as? String
    self.genderText = aDecoder.decodeObject(forKey: SerializationKeys.genderText) as? String
    self.listOfVisitedCountryIn14Days = aDecoder.decodeObject(forKey: SerializationKeys.listOfVisitedCountryIn14Days) as? String
    self.fumigationDoneText = aDecoder.decodeObject(forKey: SerializationKeys.fumigationDoneText) as? String
    self.weakness = aDecoder.decodeBool(forKey: SerializationKeys.weakness)
    self.age = aDecoder.decodeObject(forKey: SerializationKeys.age) as? Int
    self.medicinesTakenText = aDecoder.decodeObject(forKey: SerializationKeys.medicinesTakenText) as? String
    self.cityId = aDecoder.decodeObject(forKey: SerializationKeys.cityId) as? Int
    self.goesOutOfHouseText = aDecoder.decodeObject(forKey: SerializationKeys.goesOutOfHouseText) as? String
    self.stateName = aDecoder.decodeObject(forKey: SerializationKeys.stateName) as? String
    self.victimeId = aDecoder.decodeObject(forKey: SerializationKeys.victimeId) as? Int
    self.quarantineID = aDecoder.decodeObject(forKey: SerializationKeys.quarantineID) as? Int
    self.fumigationDone = aDecoder.decodeBool(forKey: SerializationKeys.fumigationDone)
    self.quarantineStampOnHand = aDecoder.decodeBool(forKey: SerializationKeys.quarantineStampOnHand)
    self.latitude = aDecoder.decodeObject(forKey: SerializationKeys.latitude) as? Int
    self.traceble = aDecoder.decodeBool(forKey: SerializationKeys.traceble)
    self.haveYouContactWithConfirmedCasesText = aDecoder.decodeObject(forKey: SerializationKeys.haveYouContactWithConfirmedCasesText) as? String
    self.haveYouBeenExplainedAfterQuarantine = aDecoder.decodeBool(forKey: SerializationKeys.haveYouBeenExplainedAfterQuarantine)
    self.suppliesKitNeeded = aDecoder.decodeBool(forKey: SerializationKeys.suppliesKitNeeded)
    self.anyDoctorConsulted = aDecoder.decodeBool(forKey: SerializationKeys.anyDoctorConsulted)
    self.lunchDinerNeededText = aDecoder.decodeObject(forKey: SerializationKeys.lunchDinerNeededText) as? String
    self.middleName = aDecoder.decodeObject(forKey: SerializationKeys.middleName) as? String
    self.wearsMask = aDecoder.decodeBool(forKey: SerializationKeys.wearsMask)
    self.wearsMaskText = aDecoder.decodeObject(forKey: SerializationKeys.wearsMaskText) as? String
    self.kitRecieved = aDecoder.decodeBool(forKey: SerializationKeys.kitRecieved)
    self.isDelete = aDecoder.decodeBool(forKey: SerializationKeys.isDelete)
    self.lunchDinerNeeded = aDecoder.decodeBool(forKey: SerializationKeys.lunchDinerNeeded)
    self.maintainDistanceMeterText = aDecoder.decodeObject(forKey: SerializationKeys.maintainDistanceMeterText) as? String
    self.callsRecievedThreeTimes = aDecoder.decodeBool(forKey: SerializationKeys.callsRecievedThreeTimes)
    self.fever = aDecoder.decodeBool(forKey: SerializationKeys.fever)
    self.weaknessText = aDecoder.decodeObject(forKey: SerializationKeys.weaknessText) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(contactNo, forKey: SerializationKeys.contactNo)
    aCoder.encode(followInstructionQuarantine, forKey: SerializationKeys.followInstructionQuarantine)
    aCoder.encode(placeVisitAfterQuarantine, forKey: SerializationKeys.placeVisitAfterQuarantine)
    aCoder.encode(haveYouContactWithConfirmedCases, forKey: SerializationKeys.haveYouContactWithConfirmedCases)
    aCoder.encode(breathingText, forKey: SerializationKeys.breathingText)
    aCoder.encode(screenedAtAirport, forKey: SerializationKeys.screenedAtAirport)
    aCoder.encode(coughText, forKey: SerializationKeys.coughText)
    aCoder.encode(feveText, forKey: SerializationKeys.feveText)
    aCoder.encode(cityName, forKey: SerializationKeys.cityName)
    aCoder.encode(haveYouBeenExplainedAfterQuarantineText, forKey: SerializationKeys.haveYouBeenExplainedAfterQuarantineText)
    aCoder.encode(aNYURTIORNOTText, forKey: SerializationKeys.aNYURTIORNOTText)
    aCoder.encode(handWashText, forKey: SerializationKeys.handWashText)
    aCoder.encode(modifiedDate, forKey: SerializationKeys.modifiedDate)
    aCoder.encode(placeVisitAfterQuarantineText, forKey: SerializationKeys.placeVisitAfterQuarantineText)
    aCoder.encode(gender, forKey: SerializationKeys.gender)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(createdDate, forKey: SerializationKeys.createdDate)
    aCoder.encode(cough, forKey: SerializationKeys.cough)
    aCoder.encode(riskPersonText, forKey: SerializationKeys.riskPersonText)
    aCoder.encode(riskPerson, forKey: SerializationKeys.riskPerson)
    aCoder.encode(suppliesKitNeededText, forKey: SerializationKeys.suppliesKitNeededText)
    aCoder.encode(medicinesTaken, forKey: SerializationKeys.medicinesTaken)
    aCoder.encode(breathingDifficulty, forKey: SerializationKeys.breathingDifficulty)
    aCoder.encode(imageData, forKey: SerializationKeys.imageData)
    aCoder.encode(currentAddress, forKey: SerializationKeys.currentAddress)
    aCoder.encode(callsRecievedThreeTimesText, forKey: SerializationKeys.callsRecievedThreeTimesText)
    aCoder.encode(aNYURTIORNOT, forKey: SerializationKeys.aNYURTIORNOT)
    aCoder.encode(permanentAddress, forKey: SerializationKeys.permanentAddress)
    aCoder.encode(lastName, forKey: SerializationKeys.lastName)
    aCoder.encode(stateId, forKey: SerializationKeys.stateId)
    aCoder.encode(forLogin, forKey: SerializationKeys.forLogin)
    aCoder.encode(tracebleText, forKey: SerializationKeys.tracebleText)
    aCoder.encode(quarantineFromDate, forKey: SerializationKeys.quarantineFromDate)
    aCoder.encode(anyDoctorConsultedText, forKey: SerializationKeys.anyDoctorConsultedText)
    aCoder.encode(quarantineStampOnHandText, forKey: SerializationKeys.quarantineStampOnHandText)
    aCoder.encode(screenedAtAirportText, forKey: SerializationKeys.screenedAtAirportText)
    aCoder.encode(policeStaionName, forKey: SerializationKeys.policeStaionName)
    aCoder.encode(createdUserId, forKey: SerializationKeys.createdUserId)
    aCoder.encode(quarantineToDate, forKey: SerializationKeys.quarantineToDate)
    aCoder.encode(goesToWorkText, forKey: SerializationKeys.goesToWorkText)
    aCoder.encode(firstName, forKey: SerializationKeys.firstName)
    aCoder.encode(goesToWork, forKey: SerializationKeys.goesToWork)
    aCoder.encode(handWash, forKey: SerializationKeys.handWash)
    aCoder.encode(maintainDistanceMeter, forKey: SerializationKeys.maintainDistanceMeter)
    aCoder.encode(policeStationId, forKey: SerializationKeys.policeStationId)
    aCoder.encode(alternatContactNo, forKey: SerializationKeys.alternatContactNo)
    aCoder.encode(followInstructionQuarantineText, forKey: SerializationKeys.followInstructionQuarantineText)
    aCoder.encode(modifiedUserId, forKey: SerializationKeys.modifiedUserId)
    aCoder.encode(goesOutOfHouse, forKey: SerializationKeys.goesOutOfHouse)
    aCoder.encode(isActive, forKey: SerializationKeys.isActive)
    aCoder.encode(kitRecievedText, forKey: SerializationKeys.kitRecievedText)
    aCoder.encode(genderText, forKey: SerializationKeys.genderText)
    aCoder.encode(listOfVisitedCountryIn14Days, forKey: SerializationKeys.listOfVisitedCountryIn14Days)
    aCoder.encode(fumigationDoneText, forKey: SerializationKeys.fumigationDoneText)
    aCoder.encode(weakness, forKey: SerializationKeys.weakness)
    aCoder.encode(age, forKey: SerializationKeys.age)
    aCoder.encode(medicinesTakenText, forKey: SerializationKeys.medicinesTakenText)
    aCoder.encode(cityId, forKey: SerializationKeys.cityId)
    aCoder.encode(goesOutOfHouseText, forKey: SerializationKeys.goesOutOfHouseText)
    aCoder.encode(stateName, forKey: SerializationKeys.stateName)
    aCoder.encode(victimeId, forKey: SerializationKeys.victimeId)
    aCoder.encode(quarantineID, forKey: SerializationKeys.quarantineID)
    aCoder.encode(fumigationDone, forKey: SerializationKeys.fumigationDone)
    aCoder.encode(quarantineStampOnHand, forKey: SerializationKeys.quarantineStampOnHand)
    aCoder.encode(latitude, forKey: SerializationKeys.latitude)
    aCoder.encode(traceble, forKey: SerializationKeys.traceble)
    aCoder.encode(haveYouContactWithConfirmedCasesText, forKey: SerializationKeys.haveYouContactWithConfirmedCasesText)
    aCoder.encode(haveYouBeenExplainedAfterQuarantine, forKey: SerializationKeys.haveYouBeenExplainedAfterQuarantine)
    aCoder.encode(suppliesKitNeeded, forKey: SerializationKeys.suppliesKitNeeded)
    aCoder.encode(anyDoctorConsulted, forKey: SerializationKeys.anyDoctorConsulted)
    aCoder.encode(lunchDinerNeededText, forKey: SerializationKeys.lunchDinerNeededText)
    aCoder.encode(middleName, forKey: SerializationKeys.middleName)
    aCoder.encode(wearsMask, forKey: SerializationKeys.wearsMask)
    aCoder.encode(wearsMaskText, forKey: SerializationKeys.wearsMaskText)
    aCoder.encode(kitRecieved, forKey: SerializationKeys.kitRecieved)
    aCoder.encode(isDelete, forKey: SerializationKeys.isDelete)
    aCoder.encode(lunchDinerNeeded, forKey: SerializationKeys.lunchDinerNeeded)
    aCoder.encode(maintainDistanceMeterText, forKey: SerializationKeys.maintainDistanceMeterText)
    aCoder.encode(callsRecievedThreeTimes, forKey: SerializationKeys.callsRecievedThreeTimes)
    aCoder.encode(fever, forKey: SerializationKeys.fever)
    aCoder.encode(weaknessText, forKey: SerializationKeys.weaknessText)
  }

}
