//
//  APIManager.swift
//  IgniteApp
//
//  Created by VBT Mac on 21/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class APIManager: NSObject {
    
    static let shared = APIManager()
    
    func request(apiURL: String, method: HTTPMethod, param: [String: String], responseBlock: @escaping ((_ result: Bool, _ jsonResponse: JSON?) -> Void)){
        
        if(Reachability.isConnectedToInternet()) {
            showLoader()
            
            Alamofire.request(apiURL, method: method, parameters: param).responseJSON { response in

                let apiUrl = (response.request?.url?.absoluteString)!
                
                if(apiUrl.contains(OTP_API_URL)){
                    dismissLoader()
                    responseBlock(true, nil)
                }
                else {
                    if let responseData = response.result.value {
                        dismissLoader()
                        let jsonResponse = JSON(responseData)
                        if(jsonResponse["Success"].intValue == 1){
                            responseBlock(true, jsonResponse)
                        }
                        else {
                            responseBlock(false, jsonResponse)
//                            Utility.shared.showAlert(titleStr: "", messageStr: jsonResponse["content"].stringValue) {
//                            }
//                            Utility.shared.showAlert(titleStr: "Something went wrong\nPlease try again.", messageStr: "") {
//                            }
                        }
                    }
                    else {
                        dismissLoader()
                        Utility.shared.showAlert(titleStr: "Could not get data\nPlease try again.", messageStr: "") {
                        }
                        responseBlock(false, nil)
                    }
                }
            }
            
//            Alamofire.request(apiURL, method: .post, parameters: ["":""], encoding: URLEncoding.queryString, headers: ["": ""]).responseJSON { (response) in
//
//                if let value = response.result.value {
//                    let json = JSON(value)
//                }
//            }
        }
        else {
            Utility.shared.showAlertForInternetConnection()
            responseBlock(false, nil)
        }
    }
    
    func urlRequest(apiUrl: String, method: String, param: [String: String], responseBlock: @escaping ((_ result: Bool, _ jsonResponse: JSON?) -> Void)) {
        if(Reachability.isConnectedToInternet()) {
                        
            guard let apiURL = URL(string: POST_API_BASE_URL) else {
                return
            }
            
            showLoader()
            
            var urlRequest = URLRequest(url: apiURL)
            urlRequest.httpMethod = method.capitalized
            //            urlRequest.allHTTPHeaderFields = ["Content-Type": "application/json"]
            urlRequest.httpBody = JSON(param).rawString()!.data(using: String.Encoding.utf8)

            let session = URLSession.shared
            let task = session.dataTask(with: urlRequest) { (data, response, error) in

                DispatchQueue.main.async(execute: {
                    dismissLoader()
                })

                guard let responseData = data else {
                    return
                }

                DispatchQueue.main.async(execute: {
                    dismissLoader()
                    do {
                        guard let jsonResponse = try JSONSerialization.jsonObject(with: responseData, options: []) as? [String: Any] else {
                            return
                        }

                        let json = JSON(jsonResponse)
                        responseBlock(true, json)
//                        if(json["InsertImageDataResult"]["Success"].intValue == 1){
//                            responseBlock(true, json)
//                        }
//                        else {
//                            Utility.shared.showAlert(titleStr: "Error", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
//
//                            }
//                        }
                    }
                    catch {
                        return
                    }
                })
            }
            task.resume()
        }
        else {
            Utility.shared.showAlertForInternetConnection()
            responseBlock(false, nil)
        }
        
    }

}

struct APIUrl {
    static let Login = String(format: "%@/apiEmployeeLogin/GetLoginEmployee", GET_API_BASE_URL)
    static let SendOTP = OTP_API_URL
    static let LatLong = String(format: "%@/apiGetLatLong/GetLatitudeLongitude", GET_API_BASE_URL)
}

