//
//  LocationClass.swift
//  BeautiMaps
//
//  Created by VBT Mac on 17/10/19.
//  Copyright © 2019 VBT Mac. All rights reserved.
//

import UIKit
import CoreLocation

class LocationClass: NSObject, CLLocationManagerDelegate {
    
    static var shared = LocationClass()
    
    var locationManager = CLLocationManager()
    
    //Location Variable
    var locationAuthorizationStatus = CLLocationManager.authorizationStatus()
    var appLocationPermission = false
    var currentLocation : CLLocation!
    var currentLatitude =  "" //"23.040060"
    var currentLongitude = "" //"72.666634"
    
    //MARK:- Location Permission
    func locationPermission() {
        locationManager = CLLocationManager()
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.requestAlwaysAuthorization()
            locationManager.delegate = self
        }
    }
    
    func updateLocationAuthStatus() {
        locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        if(locationAuthorizationStatus == .authorizedAlways || locationAuthorizationStatus == .authorizedWhenInUse){
            appLocationPermission = true
        }
        else {
            appLocationPermission = false
        }
    }
    
    func locationPermissionAlert() {
        let alert = UIAlertController(title: "App Permission Denied", message: "To re-enable, please go to Settings and turn on Location Service for this app.", preferredStyle: .alert)
        //                alert.addAction(UIAlertAction(title: "Later", style: .default, handler: { (UIAlertAction) in
        //                    alert.dismiss(animated: true, completion: nil)
        //                }))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { (UIAlertAction) in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
        }))
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindow.Level.alert + 1
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Update Current Location
    func updateCurrentLocation() {
        if(CLLocationManager.locationServicesEnabled()) {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
//            locationManager.distanceFilter = 10
            
            updateLocationAuthStatus()
            
            if(locationAuthorizationStatus != .authorizedAlways && locationAuthorizationStatus != .authorizedWhenInUse){
                locationPermissionAlert()
            }
            else {
                appLocationPermission = true
            }
        }
    }
    
    //MARK:- Get Location Distance
    func getLocationDistance(latitude: CLLocationDegrees, longitude: CLLocationDegrees) -> Double {
        let apiLocation = CLLocation(latitude: latitude, longitude: longitude)
        
        return Double(apiLocation.distance(from: currentLocation)) / 1000
    }
    
    //MARK:- CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            currentLocation = location
            currentLatitude = "\(currentLocation.coordinate.latitude)"
            currentLongitude = "\(currentLocation.coordinate.longitude)"
            locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        updateLocationAuthStatus()
        if(status == .authorizedAlways || status == .authorizedWhenInUse) {
            updateCurrentLocation()
        }
        else if(status == .denied){
            updateCurrentLocation()
        }
//        else {
//            locationPermissionAlert()
//        }
    }

}
