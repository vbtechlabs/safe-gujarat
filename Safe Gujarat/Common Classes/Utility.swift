//
//  Utility.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit
import Foundation
import SKActivityIndicatorView

class Utility: NSObject {
    
    static var shared = Utility()
    
    var window = (UIApplication.shared.delegate as? AppDelegate)?.window
    
    func saveUserDataInUserDefault(userInfoModel : UserInfoModel) {
        if #available(iOS 12.0, *) {
            // use iOS 12-only feature
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: userInfoModel, requiringSecureCoding: false)
                USER_DEFAULT.set(data, forKey: USER_DATA)
                USER_DEFAULT.synchronize()
                AppDelegate.shared.userDataJSON = JSON(USER_DEFAULT.data(forKey: USER_DATA) as Any)
            }
            catch {
                print(error.localizedDescription)
                return
            }
        } else {
            // handle older versions
            let data = NSKeyedArchiver.archivedData(withRootObject: userInfoModel)
            USER_DEFAULT.set(data, forKey: USER_DATA)
            USER_DEFAULT.synchronize()
            AppDelegate.shared.userDataJSON = JSON(USER_DEFAULT.data(forKey: USER_DATA) as Any)
        }
    }
    
    func makeRootViewController() {
        if let data = USER_DEFAULT.data(forKey: USER_DATA) {
//            AppDelegate.shared.userInfoData = UserInfoModel.init(json: JSON(data))
            AppDelegate.shared.userDataJSON = JSON(data)
            
            LocationClass.shared.updateCurrentLocation()
            
            DBOperations.shared.dbConn()
            
            WINDOW.rootViewController = MainStoryBoard.instantiateViewController(withIdentifier: "homeRoot")
        }
        else {
            WINDOW.rootViewController = MainStoryBoard.instantiateViewController(withIdentifier: "loginRoot")
        }
    }
    
    func logoutUser() {
        let domain = Bundle.main.bundleIdentifier!
        USER_DEFAULT.removePersistentDomain(forName: domain)
        USER_DEFAULT.synchronize()
        
        WINDOW.rootViewController = MainStoryBoard.instantiateViewController(withIdentifier: "loginRoot")
    }
    
    func showAlert(titleStr: String?, messageStr: String?, completionBlock: @escaping () -> Void) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            completionBlock()
            alert.dismiss(animated: true, completion: nil)
        }))
        WINDOW.rootViewController?.present(alert, animated: true, completion: nil)
//        AppDelegate.shared.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithAction(titleStr: String?, messageStr: String?, buttonActions: [String]? = nil, completionBlock: @escaping (_ index: Int, _ title: String) -> Void) {
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: .alert)
//        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
//            alert.dismiss(animated: true, completion: nil)
//        }))
        if let buttonActionList = buttonActions {
            for i in 0..<buttonActionList.count {
                alert.addAction(UIAlertAction(title: buttonActionList[i], style: .default, handler: { (alertAction) in
                    completionBlock(i, buttonActionList[i])
                }))
            }
        }
        
        WINDOW.rootViewController?.present(alert, animated: true, completion: nil)
//        AppDelegate.shared.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func showAlertForInternetConnection() {
        let alert = UIAlertController(title: "No Internet", message: "Please check your internet connection or try again later...", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (UIAlertAction) in
            alert.dismiss(animated: true, completion: nil)
        }))
        WINDOW.rootViewController?.present(alert, animated: true, completion: nil)
//        AppDelegate.shared.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func setTimeInLabel(label: UILabel, seconds: Int) {
        let hour = seconds / 3600
        let minute = seconds / 60 % 60
        let second = seconds % 60

        label.text = String(format: "%02i:%02i:%02i", hour, minute, second)
    }
}

func showLoader() {
    WINDOW.isUserInteractionEnabled = false
    //    if(kUserDefault.string(forKey: kThemeMode)! == "dark"){
    //        SKActivityIndicator.spinnerColor(Color.Pista)
    //        SKActivityIndicator.statusTextColor(Color.Pista)
    //    }
    //    else {
    //        SKActivityIndicator.spinnerColor(Color.Pink)
    //        SKActivityIndicator.statusTextColor(Color.Pink)
    //
//    SKActivityIndicator.statusLabelFont(ProximaNovaBold.Small!)
//    if(screenWidth > 380){
//        SKActivityIndicator.statusLabelFont(ProximaNovaBold.Regular!)
//    }
//    SKActivityIndicator.spinnerColor(Color.Pink)
//    SKActivityIndicator.statusTextColor(Color.Pink)
    
    SKActivityIndicator.show("Please wait")
}

func dismissLoader() {
    DispatchQueue.main.async {
        WINDOW.isUserInteractionEnabled = true
    }
    SKActivityIndicator.dismiss()
}

func getDatesDifferentForSYNCUpdates(lastUpdatedDate: String, newUpdatedDate: String) -> Bool {
    if(lastUpdatedDate != "" && newUpdatedDate != ""){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let oldDate = dateFormatter.date(from: lastUpdatedDate)!
        let newDate = dateFormatter.date(from: newUpdatedDate)!
        
        let second = Calendar.current.dateComponents([.second], from: oldDate, to: newDate).second ?? 0
        return (second > 0) ? true : false
    }
    return false
}

func checkFileExistsOnServer(url : URL) -> Bool {
    return false
}

//MARK:- API Calling
/*
 if(Reachability.isConnectedToInternet()){
     let param = [
         "school_id": txtSchoolId.text!,
         "username": txtUserName.text!,
         "password": txtPassword.text!
     ]
     
     Alamofire.request(loginAPI, method: .post, parameters: param).responseJSON { response in
     
         self.activityIndicator.stopAnimating()
         
         if let responseData = response.result.value {
             let jsonResponse = JSON(responseData)
             
             print(jsonResponse)
         }
         else {
             Utility.shared.showAlert(titleStr: "Test Alert", messageStr: "") { (index, title) in
                 print(index, title)
             }
         }
     }
 }
 else {
    Utility.shared.showAlertForInternetConnection()
 }
 */
