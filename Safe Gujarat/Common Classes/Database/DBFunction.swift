//
//  DBFunction.swift
//  Fortin Salon
//
//  Created by Prashant Belani on 15/11/18.
//  Copyright © 2018 Prashant Belani. All rights reserved.
//

import UIKit
import SQLite3

class DBFunction: NSObject {
    
    static var shared = DBFunction()
    
    let TBL_NAME = "tbl_notification"
    
    let KEY_ID = "id"
    let KEY_NOTIFICATION_TITLE = "notification_title"
    let KEY_NOTIFICATION_MESSAGE = "notification_message"
    let KEY_INSERTED_TIME = "inserted_time"
    
    //MARK:- Create Notification Table
    func createNotificationTable() -> Bool{
        return DBOperations.shared.executeQuery(queryString: String(format: "CREATE TABLE IF NOT EXISTS %@ (%@ INTEGER PRIMARY KEY, %@ TEXT, %@ TEXT, %@ TEXT)", TBL_NAME, KEY_ID, KEY_NOTIFICATION_TITLE, KEY_NOTIFICATION_MESSAGE, KEY_INSERTED_TIME))
    }
    
    //MARK:- Insert Notification
    func insertNotification(notificationTitle: String, notificationMessage: String) -> Bool {
        
        if(self.createNotificationTable()){
            let insertDate = Date().toString(dateFormat: "yyyy-MM-dd HH:mm:ss")
            return DBOperations.shared.executeQuery(queryString: String(format: "INSERT INTO %@ (%@, %@, %@) VALUES ('%@', '%@', '%@')", TBL_NAME, KEY_NOTIFICATION_TITLE, KEY_NOTIFICATION_MESSAGE, KEY_INSERTED_TIME, notificationTitle, notificationMessage, insertDate))
        }
        return false
    }
    
    //MARK:- Get All Nofitication
    func getAllNofitication() -> Array<Dictionary<String, Any>>{
        return DBOperations.shared.selectData(queryStringSelect: String(format: "SELECT * FROM %@", TBL_NAME))
    }
    
}
