//
//  DBOperations.swift
//  Fortin Salon
//
//  Created by Prashant Belani on 15/11/18.
//  Copyright © 2018 Prashant Belani. All rights reserved.
//

import UIKit
import SQLite3
import Foundation

class DBOperations: NSObject {

    static var shared = DBOperations()
    static var db : OpaquePointer?
    static var stmt : OpaquePointer?
    static var conn : Int = 0
    
    //Database conn
    func dbConn() {
        let errmsg = String(cString: sqlite3_errmsg(DBOperations.db))
        let dbname : String = "safe_gujarat.rdb"
        let filemanager : FileManager = FileManager.default
        let directory = filemanager.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let dbDirectory = directory
        
        do {
            try filemanager.createDirectory(at: dbDirectory, withIntermediateDirectories: false, attributes: nil)
        }
        catch{
            print(error.localizedDescription)
        }
        
        let docURL = dbDirectory.appendingPathComponent(dbname)
        
        let bundleURL = Bundle.main.resourceURL?.appendingPathComponent(dbname)
        
        print(docURL)
        
        if filemanager.fileExists(atPath: docURL.path){
            print("Database file exist in document")
        }
        else if filemanager.fileExists(atPath: (bundleURL?.path)!) {
            try! filemanager.copyItem(at: bundleURL!, to: docURL)
            print("Database file dose not exist copy from Bundle \(errmsg)")
        }
        
//        dbOpen(path: docURL.absoluteString)
        dbOpen(path: docURL.absoluteString) { (success) in
            print(success)
        }
    }
    
    //Database Open
    func dbOpen(path:String, completionBlock: @escaping (_ success: Bool) -> Void){
        let errmsg = String(cString: sqlite3_errmsg(DBOperations.db))
        DBOperations.conn = Int(sqlite3_open(path, &DBOperations.db))
        if DBOperations.conn != SQLITE_OK{
            print("Error in opening Database \(errmsg)")
            completionBlock(false)
        }
        else{
            print("Database Open Successfully...")
            completionBlock(true)
        }
    }
    
    func dbClose(path:String, completionBlock: @escaping (_ success: Bool) -> Void){
        let errmsg = String(cString: sqlite3_errmsg(DBOperations.db))
//        DBOperations.conn = Int(sqlite3_open(path, &DBOperations.db))
        DBOperations.conn = Int(sqlite3_close(DBOperations.db))
        if DBOperations.conn != SQLITE_OK{
            print("Error in opening Database \(errmsg)")
            completionBlock(false)
        }
        else{
            print("Database Close Successfully...")
            completionBlock(true)
        }
    }
    
    //Execute Query
    func executeQuery(queryString:String) -> Bool{
        var returnType : Bool = false
        
        if DBOperations.conn == SQLITE_OK{
            if sqlite3_prepare_v2(DBOperations.db, queryString, -1, &DBOperations.stmt, nil) == SQLITE_OK{
                if sqlite3_step(DBOperations.stmt) == SQLITE_DONE {
                    print("Execute Successed")
                    returnType = true
                }
//                else{
//                    let errmsg = String(cString: sqlite3_errmsg(DBOperations.db))
//                    print(errmsg)
//                } 
            }
            sqlite3_finalize(DBOperations.stmt)
            return returnType
        }
        return false
    }
    
    //Select Data
    func selectData(queryStringSelect: String) -> Array<Dictionary<String, Any>>{
        var arr = [Any]()
        var numberCol = 0
        let errmsg = String(cString: sqlite3_errmsg(DBOperations.db))
        
        if DBOperations.conn == SQLITE_OK{
            if sqlite3_prepare_v2(DBOperations.db, queryStringSelect, -1, &DBOperations.stmt, nil) != SQLITE_OK{
                print("Stmt wrong - \(errmsg)")
            }
            while (sqlite3_step(DBOperations.stmt) == SQLITE_ROW) {
                numberCol = Int(sqlite3_column_count(DBOperations.stmt))
                
                var dic = [String: Any]()
                for i in 0..<numberCol{
                    if sqlite3_column_type(DBOperations.stmt, Int32(i)) == SQLITE_INTEGER {
                        let name = sqlite3_column_name(DBOperations.stmt, Int32(i))
                        let columnName = String(cString: name!, encoding: String.Encoding.utf8)
                        dic[columnName!] = "\(sqlite3_column_int(DBOperations.stmt, Int32(i)))"
                    }
                    else if sqlite3_column_type(DBOperations.stmt, Int32(i)) == SQLITE_FLOAT {
                        let name = sqlite3_column_name(DBOperations.stmt, Int32(i))
                        let columnName = String(cString: name!, encoding: String.Encoding.utf8)
                        //[obj setValue:sqlite3_column_double(stmt, i) forKey:columnName];
                        //[tmpObj setObject:sqlite3_column_double(stmt, i) forKey:columnName];
                        dic[columnName!] = "\(sqlite3_column_double(DBOperations.stmt, Int32(i)))"
                    }
                    else if sqlite3_column_type(DBOperations.stmt, Int32(i)) == SQLITE_TEXT {
                        let name = sqlite3_column_name(DBOperations.stmt, Int32(i))
                        let tmpStr : String = String(cString: sqlite3_column_text(DBOperations.stmt, Int32(i)))
//                        if tmpStr == nil {
//                            tmpStr = ""
//                        }
                        let columnName = String(cString: name!, encoding: .utf8)
                        dic[columnName!] = tmpStr
                    }
                    else if sqlite3_column_type(DBOperations.stmt, Int32(i)) == SQLITE_BLOB {
//                        sqlite3_column_blob(DBOperations.stmt, Int32(i))
//                        let name = sqlite3_column_name(DBOperations.stmt, Int32(i))
//                        let columnName = String(cString: name!, encoding: String.Encoding.utf8)
//                        dic[columnName!] = "\(sqlite3_column_blob(DBOperations.stmt, Int32(i)))"
                    }
                }
                arr.append(dic)
            }
        }
        return arr as! Array<Dictionary<String, Any>>
    }
}
