//
//  AppConstant.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import Foundation
import UIKit

//UserDefault Object
let USER_DEFAULT = UserDefaults.standard
let USER_ID = "user_id"
let USER_DATA = "user_data"
let FIREBASE_TOKEN = "firebase_token"

//----------------------------------------------------------------------------------------------------------------------------

//window Object
let WINDOW = (UIApplication.shared.delegate as! AppDelegate).window!
let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height

//----------------------------------------------------------------------------------------------------------------------------

//Device Object
let DEVICE_ID = (UIDevice.current.identifierForVendor?.uuidString)!

let APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String


//----------------------------------------------------------------------------------------------------------------------------

//StoryBoard Object
let MainStoryBoard = UIStoryboard(name: "Main", bundle: nil)

//----------------------------------------------------------------------------------------------------------------------------

//API Constant
let GET_API_BASE_URL = "http://safegujarat.techpolice.in/api/"
let POST_API_BASE_URL = "http://coronago.techpolice.in/ElectionService.svc/InsertImageData"
let OTP_API_URL = "https://api.msg91.com/api/v5/otp"

//----------------------------------------------------------------------------------------------------------------------------


//----------------------------------------------------------------------------------------------------------------------------
//MARK:- Database Constant

//----------------------------------------------------------------------------------------------------------------------------
//MARK:- Exam Constant

//Color Constant
struct Color {
}

//Icon Constant
struct Icon {
    
}

struct NotificationName {
    
}

struct URLs {
    static let ContactUs = "http://safegujarat.techpolice.in/Common/ContactUs"
    static let AboutUs = "http://safegujarat.techpolice.in/Common/AboutUs"
}


