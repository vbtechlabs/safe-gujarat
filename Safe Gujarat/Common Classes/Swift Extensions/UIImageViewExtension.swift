//
//  UIImageViewExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    func setImageUsingSDWebImage(_ imageURL: String, completionHandler: @escaping (_ image: UIImage?, _ error: Error?) -> Void) {
//        let imgUrl = imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        
        if let imgUrl = URL(string: imageURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!) {
            self.sd_setImage(with: imgUrl, placeholderImage: nil, options: SDWebImageOptions.continueInBackground) { (downloadedImage, error, cacheType, downloadedImageURL) in
                if let image = downloadedImage {
                    self.image = image
                    completionHandler(image, nil)
                }
                else {
//                    self.image = UIImage(named: "placeholder")
                    completionHandler(nil, error)
                }
            }
        }
        
    }
}
