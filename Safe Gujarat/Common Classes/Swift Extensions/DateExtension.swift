//
//  DateExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import Foundation

extension Date {
    
    func getCurrentDateForDBEntry() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return dateFormatter.string(from: self)
    }
    
    //This Month Start & End
    func getStartOfThisMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func getEndOfThisMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.getStartOfThisMonth())!
    }
    
    //Last Month Start & End
    func getStartOfLastMonth() -> Date {
        var component : DateComponents = Calendar.current.dateComponents([.year, .month], from: self) as DateComponents
        component.month = component.month! - 1
        return Calendar.current.date(from: component)!
    }
    
    func getEndOfLastMonth() -> Date {
        var component : DateComponents = Calendar.current.dateComponents([.year, .month], from: self) as DateComponents
        component.day = 1
        component.day = component.day! - 1
        return Calendar.current.date(from: component)!
    }
    
    //Get Months
    func getDateToDays(addDay: Int) -> Date {
        var component : DateComponents = Calendar.current.dateComponents([.year, .month, .day], from: self) as DateComponents
        component.day = component.day! + addDay
        return Calendar.current.date(from: component)!
    }
    
    func getDateToMonths(addMonths: Int) -> Date {
        var component : DateComponents = Calendar.current.dateComponents([.year, .month, .day], from: self) as DateComponents
        component.month = component.month! + addMonths
        component.day = component.day! - 1
        return Calendar.current.date(from: component)!
    }
    
    //Get Year
    func getDateToYear(addYear: Int) -> Date {
        var component : DateComponents = Calendar.current.dateComponents([.year, .month, .day], from: self) as DateComponents
        component.year = component.year! + addYear
        return Calendar.current.date(from: component)!
    }
    
    var millisecondsSince1970: String {
        return String(Int64((self.timeIntervalSince1970 * 1000.0).rounded()))
    }
    
    func differentBetweenDate(startDate: Date) -> String {
        let day = Calendar.current.dateComponents([.day], from: self, to: startDate).day ?? 0
        let hours = Calendar.current.dateComponents([.hour], from: self, to: startDate).hour ?? 0
        let minute = Calendar.current.dateComponents([.minute], from: self, to: startDate).minute ?? 0
        
        var makeText = ""
        if(day > 0){
            if(day == 1){
                makeText = String(format: "%d day", day)
            }
            else {
                makeText = String(format: "%d days", day)
            }
        }
        else if(hours > 0) {
            if(hours == 1){
                makeText = String(format: "%d hr", hours)
            }
            else {
                makeText = String(format: "%d hrs", hours)
            }
        }
        else {
            if(minute == 1){
                makeText = String(format: "%d min", minute)
            }
            else {
                makeText = String(format: "%d mins", minute)
            }
        }
        
        return makeText
    }
    
    func getHoursDifferent(startDate: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: startDate, to: self).hour ?? 0
    }
    
    func toString(dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.string(from: self)
    }
}
