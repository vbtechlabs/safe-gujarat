//
//  UITextViewExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit

extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    @IBInspectable
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderLabel.numberOfLines = 0
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.numberOfLines = 0
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
                
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.numberOfLines = 0
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            placeholderLabel.numberOfLines = 0
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        placeholderLabel.numberOfLines = 0
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        placeholderLabel.alpha = 0.8
        placeholderLabel.font = UIFont.systemFont(ofSize: 14)
//        placeholderLabel.font = FontDynamic.OxygenRegular//Font.OxygenRegular
        placeholderLabel.textColor = UIColor.darkGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}

class CustomeTextView: UITextView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //        self.textColor = UIColor.appColors.textColor
        //        self.selectedTitleColor = UIColor.appColors.selectedTitleColor
        //        self.selectedLineColor = UIColor.appColors.textBorderColor
        //        self.font = Constant.FontName.KFontMontserratMedium
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = UIFont.systemFont(ofSize: 16)
        self.cornerRadiusOnView = 4
        self.BorderWidth = 1
        self.BorderColor = UIColor.AppTheme
//        self.color = .darkGray
    }
    
}
