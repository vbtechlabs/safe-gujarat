//
//  UIButtonExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit

extension UIButton {
    func centerVertically(padding: CGFloat) {
        guard
            let imageViewSize = self.imageView?.frame.size,
            let titleLabelSize = self.titleLabel?.frame.size else {
                return
        }
        
//        let totalHeight = imageViewSize.height + titleLabelSize.height + padding
        
//        self.imageEdgeInsets = UIEdgeInsets(
//            top: -(totalHeight - imageViewSize.height),
//            left: 0.0,
//            bottom: 0.0,
//            right: -titleLabelSize.width
//        )
        
        self.titleEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: (-imageViewSize.width)+5,
            bottom: -(titleLabelSize.height),
            right: 0.0
        )
        
        self.contentEdgeInsets = UIEdgeInsets(
            top: 0.0,
            left: 0.0,
            bottom: titleLabelSize.height - 10,
            right: 0.0
        )
    }
    
    func configDownloadAllButton() {
        let attributedDownloadAll = NSAttributedString(string: "Download All", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        let attributedStop = NSAttributedString(string: "Stop", attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
        
        self.setAttributedTitle(attributedDownloadAll, for: .normal)
        self.setAttributedTitle(attributedStop, for: .selected)
        
//        self.tintColor = UIColor.clear
//        self.backgroundColor = UIColor.clear
        self.setTitleColor(UIColor.black, for: .normal)
        self.setTitleColor(UIColor.black, for: .selected)
        self.contentMode = .right
    }
}

class CustomeButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //        self.textColor = UIColor.appColors.textColor
        //        self.selectedTitleColor = UIColor.appColors.selectedTitleColor
        //        self.selectedLineColor = UIColor.appColors.textBorderColor
        //        self.font = Constant.FontName.KFontMontserratMedium
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        if(self.tag == 101){
//            let string = (self.titleLabel?.text)!
//            let attributes = [
//                NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),
//                NSAttributedString.Key.foregroundColor: UIColor.AppTheme,
//                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single
//            ] as [NSAttributedString.Key : Any]
//            let attribtedString = NSAttributedString(string: string, attributes: attributes)
//            self.setAttributedTitle(attribtedString, for: .normal)
            
//            let myNormalAttributedTitle = NSAttributedString(string: (self.titleLabel?.text)!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.AppTheme, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single])
//            self.setAttributedTitle(myNormalAttributedTitle, for: .normal)
//            let attributed = NSMutableAttributedString(string: (self.titleLabel?.text)!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.AppTheme, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.underlineStyle: NSUnderlineStyle.init(), NSAttributedString.Key.underlineColor: UIColor.AppTheme])
//            let title = (self.titleLabel?.text)!
//            let attributed = NSMutableAttributedString(string: title)
//            attributed.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.AppTheme, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16), NSAttributedString.Key.underlineStyle: NSUnderlineStyle.init(), NSAttributedString.Key.underlineColor: UIColor.AppTheme], range: NSString(string: title).range(of: title))
//            self.setAttributedTitle(attributed, for: .normal)
        }
        else if(self.tag == 102) {
            self.cornerRadiusOnView = 4
            self.backgroundColor = UIColor.AppTheme
            self.setTitleColor(UIColor.white, for: .normal)
            self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        }
        else {
            self.cornerRadiusOnView = 4
            self.backgroundColor = UIColor.AppTheme
            self.setTitleColor(UIColor.white, for: .normal)
            self.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        }
        
    }
    
}
