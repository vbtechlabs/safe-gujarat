//
//  UITableViewExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 17/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    
    func setCustomSelectionStyle() {
        self.selectionStyle = .default
        let bgView = UIView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        bgView.backgroundColor = UIColor().hexaToUIColor(hex: "ECECEC")
        self.selectedBackgroundView = bgView
//        self.backgroundColor = UIColor.clear
    }
}
