//
//  UILabelExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit

extension UILabel{
    @IBInspectable
    var defaultFont: UIFont!{
        get{
            return self.font
        }
        set{
//            self.font = FontDynamic.OxygenRegular//Font.OxygenRegular
        }
    }
}
