//
//  UIViewControllerExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 12/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setTitle(title: String) {
        self.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.black, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 20)]
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    func setCustomBackButton() {
        let backItem = UIBarButtonItem()
        backItem.image = UIImage(named: "back")
        self.navigationController?.navigationItem.leftBarButtonItem = backItem
//        self.navigationController?.navigationItem.backBarButtonItem = backItem
//        self.navigationController?.navigationItem.backBarButtonItem?.tintColor = UIColor.black
//        if let leftItems = self.navigationItem.leftBarButtonItems {
//            for item in leftItems {
//                item.tintColor = UIColor.black
//            }
//        }
//        let imageView = UIImageView(image: UIImage(named: "back"))
//        imageView.setImageColor(color: UIColor.black)
//        let yourBackImage = imageView.image
//        self.navigationController?.navigationBar.backIndicatorImage = yourBackImage
////        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = yourBackImage
//        self.navigationController?.navigationBar.backItem?.title = ""
    }
    
//    func setBackButtonIcon() {
//        let suggestImage  = UIImage(named: "back")!.withRenderingMode(.alwaysOriginal)
//        let suggestButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 40))
//        suggestButton.setImage(suggestImage, for: .normal)
//        suggestButton.addTarget(self, action: #selector(btnBackClicked), for:.touchUpInside)
//
//        let suggestButtonContainer = UIView(frame: suggestButton.frame)
//        suggestButtonContainer.addSubview(suggestButton)
//        let suggestButtonItem = UIBarButtonItem(customView: suggestButtonContainer)
//        
//        // add button shift to the side
//        self.navigationItem.leftBarButtonItem = suggestButtonItem
//    }
//    
//    @objc func btnBackClicked(_ sender: Any) {
//        print("button back is Clicked")
//        
//        self.navigationController?.popViewController(animated: true)
//    }
}
