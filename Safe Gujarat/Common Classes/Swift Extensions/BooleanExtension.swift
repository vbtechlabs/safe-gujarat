//
//  BooleanExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import Foundation

extension Bool {
    var convertToString : String! {
        var string = ""
        if(self == true){
            string = "true"
        }
        else {
            string = "false"
        }
        return string
    }
}
