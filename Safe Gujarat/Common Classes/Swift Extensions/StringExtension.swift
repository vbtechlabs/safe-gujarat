//
//  StringExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit

extension String {
    
    func isEmptyValue() -> Bool {
        return (self == "") ? true : false
    }
    
    func emailValidate() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func numberValidate() -> Bool {
        let phoneRegEx = "^((\\+)|(00))[0-9]{12}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegEx)
        return phoneTest.evaluate(with: self)
    }
    
    func passwordValidate() -> Bool {
        // at least one uppercase, at least one digit, at least one lowercase, 8 characters total
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "(?=.*[A-Z])(?=.*[$@$#!%?&])(?=.*[0-9])(?=.*[a-z]).{6,}")
        return passwordTest.evaluate(with: self)
    }
    
    var convertToBool: Bool {
        switch self.lowercased() {
        case "t", "true", "yes", "y", "on", "1", "enable":
            return true
        case "f", "false", "no", "n", "off", "0", "disable":
            return false
        default:
            return false
        }
    }
    
    var convertTo2Double: String! {
        return String(format: "%.2f", self)
    }
    
    var convertTo3Double: String! {
        return String(format: "%.3f", self)
    }
    
    func utf8DecodedString() -> String {
        let data = self.data(using: .utf8)
        if let message = String(data: data!, encoding: .nonLossyASCII){
            return message
        }
        return ""
    }
    
    func utf8EncodedString() -> String? {
        let messageData = self.data(using: .nonLossyASCII)
        let text = String(data: messageData!, encoding: .utf8)
        return text
    }
    
    var html2AttributedString: NSAttributedString? {
        return Data(utf8).html2AttributedString
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
    
    func heightOfString(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
        let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
        return String(cleanedUpCopy.enumerated().map() {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
            }.joined().dropFirst())
    }
    
    func toDate() -> Date {
        if(self == ""){
            return Date()
        }
        else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return dateFormatter.date(from: self)!
        }
    }
    
    func getDatesDifferentForSYNCUpdates(lastUpdatedDate: String, newUpdatedDate: String) -> Bool {
        if(lastUpdatedDate != "" && newUpdatedDate != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let oldDate = dateFormatter.date(from: lastUpdatedDate)!
            let newDate = dateFormatter.date(from: newUpdatedDate)!
            
            let second = Calendar.current.dateComponents([.second], from: oldDate, to: newDate).second ?? 0
            return (second > 0) ? true : false
        }
        return false
    }
    
    func createFileNameFromFileURL() -> String {
        if(self != ""){
            if(self.contains("/")){
                return String(self.split(separator: "/").last!)
            }
        }
        return self
    }
    
    func generateOTP() -> String {
        var otp = ""
        for i in 1...4{
            print(i)
            otp.append("\(Int.random(in: 0...9))")
        }
        return otp
    }
    
//    public func stringByAddingPercentEncodingForRFC3986() -> String? {
//        let unreserved = "-._~/?"
//        let allowedCharacterSet = NSMutableCharacterSet.alphanumeric()
//        allowedCharacterSet.addCharacters(in: unreserved)
//
//    }
    
//    public func stringByAddingPercentEncodingForFormData(plusForSpace: Bool=false) -> String? {
//        let unreserved = "*-._"
//        let allowedCharacterSet = NSMutableCharacterSet.alphanumericCharacterSet()
//        allowedCharacterSet.addCharactersInString(unreserved)
//
//        if plusForSpace {
//            allowedCharacterSet.addCharactersInString(" ")
//        }
//
//        var encoded = stringByAddingPercentEncodingWithAllowedCharacters(allowedCharacterSet)
//        if plusForSpace {
//            encoded = encoded?.stringByReplacingOccurrencesOfString(" ", withString: "+")
//        }
//        return encoded
//    }
}
