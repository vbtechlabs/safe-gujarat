//
//  UITextFieldExtension.swift
//  IgniteApp
//
//  Created by VBT Mac on 09/02/20.
//  Copyright © 2020 Prashant Belani. All rights reserved.
//

import UIKit

extension UITextField{
    @IBInspectable
    var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder  = NSAttributedString(string: self.placeholder != nil ? self.placeholder! : "",
                                                             attributes: [NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
    
    func setPadding(viewAlignment: String, width: CGFloat){
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: width, height: self.frame.height))
        if(viewAlignment == "left"){
            self.leftView = view
            self.leftViewMode = .always
        }
        else {
            self.rightView = view
            self.rightViewMode = .always
        }
    }
    
//    func setLeftView(image: UIImage){
//        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 34, height: 40))
//        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 24.0, height: 24.0)))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        imageView.center = view.center
//        view.addSubview(imageView)
//        self.leftView = view
//        self.leftViewMode = .always
//    }
//
//    func setRightView(image: UIImage, imageTintColor: UIColor?){
//        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 34, height: 40))
//        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 24.0, height: 24.0)))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        imageView.center = view.center
//        view.addSubview(imageView)
//        self.rightView = view
//        self.rightViewMode = .always
//    }
    
//    func setRightViewForCustomTextField(image: UIImage){
//        let view = UIView(frame: CGRect(x: 0.0, y: 6.0, width: 34, height: 34))
//        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 24.0, height: 24.0)))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        imageView.center = view.center
//        view.addSubview(imageView)
//        self.rightView = view
//        self.rightViewMode = .always
//    }
    
//    func setLeftViewWithCustomSize(image: UIImage, viewSize: CGSize, imageViewSize: CGSize){
//        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: viewSize.width, height: viewSize.height))
//        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: imageViewSize))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        imageView.center = view.center
//        view.addSubview(imageView)
//        self.leftView = view
//        self.leftViewMode = .always
//    }
//
//    func setRightViewWithCustomSize(image: UIImage, viewSize: CGSize, imageViewSize: CGSize){
//        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: viewSize.width, height: viewSize.height))
//        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: imageViewSize))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        imageView.center = view.center
//        view.addSubview(imageView)
//        self.rightView = view
//        self.rightViewMode = .always
//    }
    
    func setLeftRightViewWithCustom(viewAllignment: String, image: UIImage, imageColor: UIColor?, viewSize: CGSize?, imageViewSize: CGSize?){
        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 34, height: 40))
        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 24.0, height: 24.0)))
        if(viewSize != nil){
            view.frame.size = viewSize!
        }
        if(imageViewSize != nil){
            imageView.frame.size = imageViewSize!
        }
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        imageView.center = view.center
        if(imageColor != nil){
            imageView.setImageColor(color: imageColor!)
        }
        view.addSubview(imageView)
        let allign = viewAllignment.lowercased()
        if(allign == "l" || allign == "left"){
            self.leftView = view
            self.leftViewMode = .always
        }
        else {
            self.rightView = view
            self.rightViewMode = .always
        }
    }
    
//    func setSmallSizeRightView(image: UIImage){
//        let view = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 17, height: 20))
//        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: 14.0, height: 14.0)))
//        imageView.contentMode = .scaleAspectFit
//        imageView.image = image
//        imageView.center = view.center
//        view.addSubview(imageView)
//        self.rightView = view
//        self.rightViewMode = .always
//    }
    
    @IBInspectable var leftPadding: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }

    @IBInspectable var rightPadding: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}

class CustomeTextField: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        //        self.textColor = UIColor.appColors.textColor
        //        self.selectedTitleColor = UIColor.appColors.selectedTitleColor
        //        self.selectedLineColor = UIColor.appColors.textBorderColor
        //        self.font = Constant.FontName.KFontMontserratMedium
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.font = UIFont.systemFont(ofSize: 16)
        self.cornerRadiusOnView = 4
        self.BorderWidth = 1
        self.BorderColor = UIColor.AppTheme
        self.placeHolderColor = .darkGray
    }
    
}
