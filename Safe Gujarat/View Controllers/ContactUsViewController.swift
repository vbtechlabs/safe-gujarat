//
//  ContactUsViewController.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 24/03/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit
import WebKit

class ContactUsViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var webView: WKWebView!
    
    var pushFrom = ""

    //MARK:- View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    //MARK:- SetupUI
    func setupUI() {
        var urlRequest : URLRequest!
        
        if(pushFrom == "contact"){
            self.title = "Contact Us"
            urlRequest = URLRequest(url: URL(string: URLs.ContactUs)!)
        }
        else if(pushFrom == "about"){
            self.title = "About Us"
            urlRequest = URLRequest(url: URL(string: URLs.AboutUs)!)
        }
        
        webView.load(urlRequest)
    }


    //MARK:- Button Action
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
