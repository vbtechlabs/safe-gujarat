//
//  LoginViewController.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 23/03/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit
import SwiftEntryKit
import SGDigitTextField

class LoginViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewContent: UIView!
    
//    @IBOutlet weak var stackViewOTPFields: UIStackView!
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var txtMobile: CustomeTextField!
    @IBOutlet weak var txtOTP: SGDigitTextField!
//    @IBOutlet weak var txtOne: CustomeTextField!
//    @IBOutlet weak var txtTwo: CustomeTextField!
//    @IBOutlet weak var txtThree: CustomeTextField!
//    @IBOutlet weak var txtFour: CustomeTextField!
    
    @IBOutlet weak var btnSendOTP: CustomeButton!
    @IBOutlet weak var btnResendOTP: UIButton!
    @IBOutlet weak var btnContactUs: UIButton!
    
    var otpNumber = ""
    
    //MARK:- View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    //MARK:- SetupUI
    func setupUI() {
        
    }
    
    func showOTPSuccessView() {
        let notificationView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 50))
        notificationView.backgroundColor = UIColor.AppTheme
        let imageView = UIImageView(frame: CGRect(x: 14, y: 0, width: 36, height: 50))
        imageView.image = UIImage(named: "smile")
        imageView.contentMode = .scaleAspectFit
        let label = UILabel(frame: CGRect(x: 60, y: 0, width: (SCREEN_WIDTH - 60), height: 50))
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.text = "OTP Send Successfully"
        label.textColor = UIColor.white
        notificationView.addSubview(imageView)
        notificationView.addSubview(label)

        var attribute = EKAttributes()
        attribute.displayDuration = 3.0
        attribute.position = .top
        attribute.windowLevel = .normal
        attribute.positionConstraints.size = .init(width: EKAttributes.PositionConstraints.Edge.fill, height: EKAttributes.PositionConstraints.Edge.constant(value: 50))


        SwiftEntryKit.display(entry: notificationView, using: attribute)
    }
    
    //MARK:- Resend OTP API Call
    func resendOTPAPICall() {
        self.otpNumber = String().generateOTP()
        print("Your OTP is \(self.otpNumber)")
        
        let param = [
            "authkey" : "318860AfpOyq3nKojh5e4b94feP1",
            "template_id": "5e4bc7b1d6fc0544b662a1b5",
            "extra_param": JSON(["COMPANY_NAME": "QUARANTINE GUJARAT App"]).rawString(String.Encoding.utf8, options: [])!,
            "mobile": txtMobile.text!,
            "otp": self.otpNumber
        ]
        
        APIManager.shared.request(apiURL: APIUrl.SendOTP, method: .get, param: param) { (success, jsonResponse) in
            if(success){
                self.showOTPSuccessView()
                
                self.txtMobile.isHidden = true
                self.txtOTP.isHidden = false
                self.btnResendOTP.isHidden = false
                
                self.btnSendOTP.setTitle("Login", for: .normal)
//                if let json = jsonResponse {
//                    print(json)
//                }
            }
        }
    }


    //MARK:- Button Action
    @IBAction func btnSendOTPIsClicked(_ sender: UIButton) {
        self.endEditing()
        if(sender.titleLabel?.text == "Send OTP"){
            if(txtMobile.text == ""){
                Utility.shared.showAlert(titleStr: "", messageStr: "Please enter mobile number") {
                }
            }
            else if(txtMobile.text!.count < 10){
                Utility.shared.showAlert(titleStr: "", messageStr: "Please enter valid mobile number") {
                }
            }
            else {
                self.resendOTPAPICall()
            }
        }
        else {
            if(txtOTP.text == ""){
                Utility.shared.showAlert(titleStr: "", messageStr: "Please enter otp number") {
                }
            }
            else if(txtOTP.text != self.otpNumber){
                Utility.shared.showAlert(titleStr: "", messageStr: "Please enter valid otp number") {
                }
            }
            else {
                let param = [
                    "ContactNum": txtMobile.text!,
                    "DeviceId": DEVICE_ID,
                    "IsAdmin": "false",
                    "FireBaseId": USER_DEFAULT.string(forKey: FIREBASE_TOKEN) ?? ""
                ]
                
                APIManager.shared.request(apiURL: APIUrl.Login, method: .get, param: param) { (success, jsonResponse) in
                    
                    guard let json = jsonResponse else { 
                        return
                    }
                    
                    if(success){
                            print(json)
//                            Utility.shared.saveUserDataInUserDefault(userInfoModel: UserInfoModel.init(json: json["content"]))
                            let userData = json["content"][0].rawString()?.data(using: String.Encoding.utf8)!
                            USER_DEFAULT.set(userData, forKey: USER_DATA)
                            USER_DEFAULT.synchronize()
                            
                            Utility.shared.makeRootViewController()
                    }
                    else {
                        Utility.shared.showAlert(titleStr: "", messageStr: json["content"].stringValue) {
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnResendOTPIsClicked(_ sender: UIButton) {
        self.endEditing()
        self.resendOTPAPICall()
    }
    
    @IBAction func btnContactUsIsClicked(_ sender: UIButton) {
        self.endEditing()
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
        vc.pushFrom = "contact"
        self.navigationController?.show(vc, sender: self)
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let text = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
        if(textField == txtMobile){
            return text.count <= 10
        }
        return true
    }
}
