//
//  OTPVerifyViewController.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 23/03/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit

class OTPVerifyViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewContent: UIView!
    
    @IBOutlet weak var imgIcon: UIImageView!
    
    @IBOutlet weak var txtOTP: CustomeTextField!
    
    @IBOutlet weak var btnVerifyOTP: CustomeButton!

    //MARK:- View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    //MARK:- SetupUI
    func setupUI() {
        
    }


    //MARK:- Button Action
    @IBAction func btnVerifyOTPIsClicked(_ sender: UIButton) {
        Utility.shared.makeRootViewController()
    }
}
