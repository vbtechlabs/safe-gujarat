//
//  HomeViewController.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 25/03/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit
import DropDown

class HomeViewController: UIViewController {

    //MARK:- Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var viewUserData: UIView!
    @IBOutlet weak var viewAttendance: UIView!
    @IBOutlet weak var viewHelp: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var imgAttandance: UIImageView!
    
    @IBOutlet weak var btnAttandance: CustomeButton!
    @IBOutlet weak var btnCancelAttandance: CustomeButton!
    @IBOutlet weak var btnAttandanceSubmit: CustomeButton!
    
    @IBOutlet weak var btnUserData: UIButton!
    @IBOutlet weak var btnNeedHelp: CustomeButton!
    @IBOutlet weak var btnCancelHelp: CustomeButton!
    @IBOutlet weak var btnSubmitHelp: CustomeButton!
    
    @IBOutlet weak var txtHelpTitle: CustomeTextField!
    
    @IBOutlet weak var txtHelpDetail: CustomeTextView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAge: UILabel!
    @IBOutlet weak var lblDetailPlaceholder: UILabel!
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    @IBOutlet weak var viewUserHeight: NSLayoutConstraint!
    @IBOutlet weak var viewAttendanceHeight: NSLayoutConstraint!
    @IBOutlet weak var viewHelpHeight: NSLayoutConstraint!
    
    let dropDownMenu = DropDown()
    var apiLat = "", apiLong = "", victimeId = ""
    
    var homeDataSource = [HomeSectionModel]()
    
    //MARK:- View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    //MARK:- SetupUI
    func setupUI() {
        self.setupDropDown()
        
        self.viewUserData.isHidden = true
        self.manageUserView()
        
        self.viewAttendance.isHidden = true
        self.manageAttandanceView()
        
        self.viewHelp.isHidden = true
        self.manageHelpView()
        
        self.txtHelpTitle.leftPadding = 8
        
        let nameString = String(format: "Name : %@", AppDelegate.shared.userDataJSON["FirstName"].stringValue)
        let nameAttribute = NSMutableAttributedString(string: nameString)
        nameAttribute.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSString(string: nameString).range(of: AppDelegate.shared.userDataJSON["FirstName"].stringValue))
        self.lblUserName.attributedText = nameAttribute
        
        let ageString = String(format: "Age : %@", AppDelegate.shared.userDataJSON["Age"].stringValue)
        let ageAttribute = NSMutableAttributedString(string: ageString)
        ageAttribute.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSString(string: ageString).range(of: AppDelegate.shared.userDataJSON["Age"].stringValue))
        self.lblUserAge.attributedText = ageAttribute
        
        self.homeDataSource.removeAll()
        homeDataSource.append(HomeSectionModel.init(id: "", title: "ATTANDANCE / હાજરી", detail: "", image: "", isExpand: "0", rowData: [HomeRowDataModel]()))

        homeDataSource.append(HomeSectionModel.init(id: "", title: "HELP / મદદ", detail: "", image: "", isExpand: "0", rowData: [HomeRowDataModel.init(id: "", title: "", detail: "", latitude: "", longtitide: "", forLogin: "", victimeId: "", age: "", image: #imageLiteral(resourceName: "camera"), isExpand: "0")]))
        
        self.callLatitudeLongitudeAPI()
    }
    
    //MARK:- Setup Drop Down
    func setupDropDown() {
        let version = "V \(APP_VERSION)"
        dropDownMenu.dataSource = ["Notification", "About Us", "Contact Us"/*, "Logout", version*/]
        dropDownMenu.width = 180
        dropDownMenu.backgroundColor = UIColor.white
        dropDownMenu.selectionBackgroundColor = UIColor.white
        dropDownMenu.selectionAction = { (index: Int, title: String) in
            if(title == "Notification"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
                self.navigationController?.show(vc, sender: self)
            }
            else if(title == "About Us"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                vc.pushFrom = "about"
                self.navigationController?.show(vc, sender: self)
            }
            else if(title == "Contact Us"){
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsViewController") as! ContactUsViewController
                vc.pushFrom = "contact"
                self.navigationController?.show(vc, sender: self)
            }
            else if(title == "Logout"){
                Utility.shared.showAlertWithAction(titleStr: "", messageStr: "Are you sure you want to logout?", buttonActions: ["No", "Yes"]) { (index, title) in
                    if(index == 1){
                        Utility.shared.logoutUser()
                    }
                }
            }
            else if(title == version){
                
            }
        }
    }
    
    func manageUserView() {
        self.viewUserHeight.constant = (viewUserData.isHidden == true) ? 0 : 50
    }
    
    func manageAttandanceView() {
        self.viewAttendanceHeight.constant = (viewAttendance.isHidden == true) ? 0 : 185
    }
    
    func manageHelpView() {
        self.viewHelpHeight.constant = (viewHelp.isHidden == true) ? 0 : 225
    }

    func callLatitudeLongitudeAPI() {
        APIManager.shared.request(apiURL: APIUrl.LatLong, method: .get, param: ["DeviceId": DEVICE_ID]) { (success, jsonResponse) in
            
            guard let json = jsonResponse else {
                return
            }
            
            if(success){
                print(json)
                self.apiLat = json["content"][0]["Latitude"].stringValue
                self.apiLong = json["content"][0]["Longitude"].stringValue
                self.victimeId = json["content"][0]["VictimeId"].stringValue
                
                let content = json["content"]
                var rowData = [HomeRowDataModel]()
                for i in 0..<content.count {
                    rowData.append(HomeRowDataModel.init(id: "", title: content[i]["FirstName"].stringValue, detail: content[i][""].stringValue, latitude: content[i]["Latitude"].stringValue, longtitide: content[i]["Longitude"].stringValue, forLogin: content[i]["For_Login"].stringValue, victimeId: content[i]["VictimeId"].stringValue, age: content[i]["Age"].stringValue, image: #imageLiteral(resourceName: "camera"), isExpand: "0"))
                }
                let index = self.homeDataSource.firstIndex(of: self.homeDataSource.filter({$0.title == "ATTANDANCE / હાજરી"}).first!) ?? 0
                self.homeDataSource[index].rowData = rowData
            }
        }
    }
    
    func submitAttandanceAPICall(rowIndex: Int) {
        let rowData = self.homeDataSource[0].rowData[rowIndex]
        var attandanceImage : String {
            if(rowData.image != #imageLiteral(resourceName: "camera")){
                return (rowData.image.jpegData(compressionQuality: 0.05)?.base64EncodedString(options: .lineLength64Characters))!
            }
            return ""
        }
        
        var statusId : String {
            if(LocationClass.shared.getLocationDistance(latitude: Double(rowData.latitude)!, longitude: Double(rowData.longtitide)!) <= 450){
                return "1"
            }
            return "2"
        }
        
        showLoader()
        
        let param = [
            "VictimeId": rowData.victimeId,
            "StatusId": statusId,
            "Latitude": LocationClass.shared.currentLatitude,
            "Longitude": LocationClass.shared.currentLongitude,
            "Image1": attandanceImage,
            "Image2": "",
            "Image3": "",
            "Image4": "",
            "UserId": AppDelegate.shared.userDataJSON["CreatedUserId"].stringValue,
            "PostTypeId": "3"
        ]
        
        APIManager.shared.urlRequest(apiUrl: POST_API_BASE_URL, method: "POST", param: param) { (success, jsonResponse) in
            if(success){
                if let json = jsonResponse {
                    if(json["InsertImageDataResult"]["Success"].intValue == 1){
                        Utility.shared.showAlert(titleStr: "Success", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                            self.homeDataSource[0].rowData[rowIndex].image = #imageLiteral(resourceName: "camera")
                            self.homeDataSource[0].rowData[rowIndex].isExpand = "0"
                            self.tableView.reloadRows(at: [IndexPath(row: rowIndex, section: 0)], with: .none)
                        }
                    }
                    else {
                        Utility.shared.showAlert(titleStr: "Error", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                            
                        }
                    }
                }
            }
        }
    }
    
    func submitHelpAPICall(rowIndex: Int) {
        let param = [
            "VictimeId": self.victimeId,
            "Ward": self.homeDataSource[1].rowData[rowIndex].title,
            "CurrentAddress": self.homeDataSource[1].rowData[rowIndex].detail,
            "PostTypeId": "7"
        ]
        
        APIManager.shared.urlRequest(apiUrl: POST_API_BASE_URL, method: "POST", param: param) { (success, jsonResponse) in
            if(success){
                if let json = jsonResponse {
                    if(json["InsertImageDataResult"]["Success"].intValue == 1){
                        Utility.shared.showAlert(titleStr: "Success", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                            self.homeDataSource[1].rowData[0].title = ""
                            self.homeDataSource[1].rowData[0].detail = ""
                            self.homeDataSource[1].isExpand = "0"
                            self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
                        }
                    }
                    else {
                        Utility.shared.showAlert(titleStr: "Error", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                        }
                    }
                }
            }
        }
    }

    //MARK:- Button Action
    @IBAction func btnUserDataClicked(_ sender: UIButton) {
        self.viewAttendance.isHidden = false
        self.manageAttandanceView()
        
        ImagePicker.shared.showImagePicker(self, sender: self.view) { (imagePicker, pickerInfo) -> (Void) in
            if let pickInfo = pickerInfo {
                if let pickedImage = pickInfo[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                    self.imgAttandance.image = pickedImage
                }
            }
        }
    }
    
    @IBAction func btnAttandanceClicked(_ sender: UIButton) {
        self.viewUserData.isHidden = false
        self.manageUserView()
        
//        ImagePicker.shared.showImagePicker(self, sender: self.view) { (imagePicker, pickerInfo) -> (Void) in
//            if let pickInfo = pickerInfo {
//                if let pickedImage = pickInfo[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//                    self.imgAttandance.image = pickedImage
//                }
//            }
//        }
    }
    
    @IBAction func btnSubmitAttandanceClicked(_ sender: UIButton) {
        var attandanceImage : String {
            if(self.imgAttandance != UIImage(named: "camera")){
                return (self.imgAttandance.image?.jpegData(compressionQuality: 0.7)?.base64EncodedString(options: .lineLength64Characters))!
            }
            return ""
        }
        
        var statusId : String {
            if(LocationClass.shared.getLocationDistance(latitude: Double(self.apiLat)!, longitude: Double(self.apiLong)!) <= 450){
                return "1"
            }
            return "2"
        }
        
        showLoader()
        
        let param = [
            "VictimeId": self.victimeId,
            "StatusId": statusId,
            "Latitude": LocationClass.shared.currentLatitude,
            "Longitude": LocationClass.shared.currentLongitude,
            "Image1": attandanceImage,
            "Image2": "",
            "Image3": "",
            "Image4": "",
            "UserId": AppDelegate.shared.userDataJSON["CreatedUserId"].stringValue,
            "PostTypeId": "3"
        ]
        
        APIManager.shared.urlRequest(apiUrl: POST_API_BASE_URL, method: "POST", param: param) { (success, jsonResponse) in
            if(success){
                if let json = jsonResponse {
                    if(json["InsertImageDataResult"]["Success"].intValue == 1){
                        Utility.shared.showAlert(titleStr: "Success", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                            self.imgAttandance.image = UIImage(named: "camera")
                            self.viewAttendance.isHidden = true
                            self.manageAttandanceView()
                        }
                    }
                    else {
                        Utility.shared.showAlert(titleStr: "Error", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                            
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnCancelAttandanceClicked(_ sender: UIButton) {
        self.viewAttendance.isHidden = true
        self.manageAttandanceView()
        
        self.viewUserData.isHidden = true
        self.manageUserView()
    }
    
    @IBAction func btnNeedHelpClicked(_ sender: UIButton) {
        self.viewHelp.isHidden = false
        self.manageHelpView()
    }
    
    @IBAction func btnSubmitHelpClicked(_ sender: UIButton) {
        let param = [
            "VictimeId": self.victimeId,
            "Ward": txtHelpTitle.text!,
            "CurrentAddress": txtHelpDetail.text!,
            "PostTypeId": "7"
        ]
        
        APIManager.shared.urlRequest(apiUrl: POST_API_BASE_URL, method: "POST", param: param) { (success, jsonResponse) in
            if(success){
                if let json = jsonResponse {
                    if(json["InsertImageDataResult"]["Success"].intValue == 1){
                        Utility.shared.showAlert(titleStr: "Success", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                            self.txtHelpTitle.text = ""
                            self.txtHelpDetail.text = ""
                            self.lblDetailPlaceholder.isHidden = false
                            self.viewHelp.isHidden = true
                            self.manageHelpView()
                        }
                    }
                    else {
                        Utility.shared.showAlert(titleStr: "Error", messageStr: json["InsertImageDataResult"]["Exception"].stringValue) {
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func btnCancelHelpClicked(_ sender: UIButton) {
        self.viewHelp.isHidden = true
        self.manageHelpView()
    }
    
    @IBAction func btnMenuClicked(_ sender: UIBarButtonItem) {
        dropDownMenu.anchorView = sender.plainView
        dropDownMenu.show()
    }
    
}

extension HomeViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
//        let text = NSString(string: textView.text).replacingCharacters(in: range, with: text)
        if(textField.tag == 1000){
            self.homeDataSource[1].rowData[0].title = textField.text!
        }
    }
}

extension HomeViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let text = NSString(string: textView.text).replacingCharacters(in: range, with: text)
        if(textView.tag == 100){
            self.homeDataSource[1].rowData[0].detail = text
            let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! HomeHelpTableViewCell
            cell.lblDetailPlaceholder.isHidden = text.count > 0
        }
        return true
    }
}

//MARK:- TableView Data Source & Delegate
extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return homeDataSource.count
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HomeTableHeaderView", owner: tableView, options: nil)?.first as! HomeTableHeaderView
        headerView.button.setTitle(homeDataSource[section].title, for: .normal)
        headerView.button.action(.touchUpInside) { (sender) in
            self.homeDataSource[section].isExpand = (self.homeDataSource[section].isExpand == "0") ? "1" : "0"
            tableView.layoutIfNeeded()
            tableView.reloadSections(IndexSet(integer: section), with: .none)
        }
        return headerView
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (homeDataSource[section].isExpand.convertToBool == true) ? homeDataSource[section].rowData.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section, row = indexPath.row
        let cellId = String(format: "cellId%d%d", section, row)
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        if(self.homeDataSource[section].title == "ATTANDANCE / હાજરી"){
            var cellObj = tableView.dequeueReusableCell(withIdentifier: cellId) as? HomeAttandanceTableViewCell
            if(cell == nil){
                tableView.register(UINib(nibName: "HomeAttandanceTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
                cellObj = tableView.dequeueReusableCell(withIdentifier: cellId) as? HomeAttandanceTableViewCell
            }
            
            let rowData = self.homeDataSource[section].rowData[row]
            
            let nameString = String(format: "Name : %@", rowData.title)
            let nameAttribute = NSMutableAttributedString(string: nameString)
            nameAttribute.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSString(string: nameString).range(of: rowData.title))
            cellObj?.lblUserName.attributedText = nameAttribute
            
            let ageString = String(format: "Age : %@", rowData.age)
            let ageAttribute = NSMutableAttributedString(string: ageString)
            ageAttribute.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.black], range: NSString(string: ageString).range(of: rowData.age))
            cellObj?.lblUserAge.attributedText = ageAttribute
            
            cellObj?.imgAttandance.image = rowData.image
            
            if(rowData.isExpand.convertToBool){
                cellObj?.viewAttendanceHeight.constant = 185
                cellObj?.viewAttendance.isHidden = false
            }
            else {
                cellObj?.viewAttendanceHeight.constant = 0
                cellObj?.viewAttendance.isHidden = true
            }
            
            cellObj?.btnUserData.action(.touchUpInside, { (sender) in
                self.endEditing()
                if(self.homeDataSource[section].rowData[row].isExpand == "0"){
                    self.homeDataSource[section].rowData[row].isExpand = "1"
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
                
                ImagePicker.shared.showImagePicker(self, sender: self.view) { (imagePicker, pickerInfo) -> (Void) in
                    if let pickInfo = pickerInfo {
                        if let pickedImage = pickInfo[UIImagePickerController.InfoKey.originalImage] as? UIImage {
//                            cellObj?.imgAttandance.image = pickedImage
                            self.homeDataSource[section].rowData[row].image = pickedImage
                            self.tableView.reloadRows(at: [indexPath], with: .none)
                        }
                    }
                }
            })
            
            cellObj?.btnSubmit.action(.touchUpInside, { (sender) in
                self.endEditing()
                self.submitAttandanceAPICall(rowIndex: row)
            })
            
            cellObj?.btnCancel.action(.touchUpInside, { (sender) in
                self.endEditing()
                if(self.homeDataSource[section].rowData[row].isExpand == "1"){
                    self.homeDataSource[section].rowData[row].isExpand = "0"
                    tableView.reloadRows(at: [indexPath], with: .none)
                }
            })
            
            cell = cellObj
        }
        else if(self.homeDataSource[section].title == "HELP / મદદ"){
            var cellObj = tableView.dequeueReusableCell(withIdentifier: cellId) as? HomeHelpTableViewCell
            if(cell == nil){
                tableView.register(UINib(nibName: "HomeHelpTableViewCell", bundle: nil), forCellReuseIdentifier: cellId)
                cellObj = tableView.dequeueReusableCell(withIdentifier: cellId) as? HomeHelpTableViewCell
            }
            let sectionData = self.homeDataSource[section]
            let rowData = self.homeDataSource[section].rowData[row]
            
            if(sectionData.isExpand.convertToBool){
                cellObj?.viewHelpHeight.constant = 245
                cellObj?.viewHelp.isHidden = false
            }
            else {
                cellObj?.viewHelpHeight.constant = 0
                cellObj?.viewHelp.isHidden = true
            }
            
            cellObj?.txtHelpTitle.text = rowData.title
            cellObj?.txtHelpTitle.tag = 1000
            cellObj?.txtHelpTitle.delegate = self
            
            cellObj?.txtHelpDetail.text = rowData.detail
            cellObj?.txtHelpDetail.tag = 100
            cellObj?.txtHelpDetail.delegate = self
            
            cellObj?.lblDetailPlaceholder.isHidden = (rowData.detail.count) > 0
            
            cellObj?.btnSubmit.action(.touchUpInside, { (sender) in
                self.endEditing()
                self.submitHelpAPICall(rowIndex: row)
            })
            
            cellObj?.btnCancel.action(.touchUpInside, { (sender) in
                self.endEditing()
                if(self.homeDataSource[section].isExpand == "1"){
                    self.homeDataSource[section].isExpand = "0"
                    tableView.reloadSections(IndexSet(integer: section), with: .none)
                }
            })
            
            cell = cellObj
        }
        
        return cell!
    }
}
