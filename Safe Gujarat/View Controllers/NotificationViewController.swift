//
//  NotificationViewController.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 26/03/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    //MARK:- Outlets
    
    var arrayNotification = [NotificationModel]()

    //MARK:- View Controller Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupUI()
    }

    //MARK:- SetupUI
    func setupUI() {
        self.getNotificationFromDB()
    }
    
    //MARK:- Get Notification From DB
    func getNotificationFromDB() {
        let notificationData = JSON(DBFunction.shared.getAllNofitication())
        
        self.arrayNotification.removeAll()
        for i in 0..<notificationData.count {
            self.arrayNotification.append(NotificationModel.init(id: notificationData[i][DBFunction.shared.KEY_ID].stringValue, title: notificationData[i][DBFunction.shared.KEY_NOTIFICATION_TITLE].stringValue, message: notificationData[i][DBFunction.shared.KEY_NOTIFICATION_MESSAGE].stringValue, insertedDate: notificationData[i][DBFunction.shared.KEY_INSERTED_TIME].stringValue))
        }
    }


    //MARK:- Button Action
    @IBAction func btnBackClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }

}
