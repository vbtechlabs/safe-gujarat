//
//  HomeAttandanceTableViewCell.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 01/04/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit

class HomeAttandanceTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var viewUserData: UIView!
    @IBOutlet weak var viewAttendance: UIView!
    
    @IBOutlet weak var imgAttandance: UIImageView!

    @IBOutlet weak var btnCancel: CustomeButton!
    @IBOutlet weak var btnSubmit: CustomeButton!
    @IBOutlet weak var btnUserData: UIButton!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserAge: UILabel!
    
    @IBOutlet weak var viewAttendanceHeight: NSLayoutConstraint!
    @IBOutlet weak var viewAttendanceBottom: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
