//
//  HomeHelpTableViewCell.swift
//  Safe Gujarat
//
//  Created by VBT Mac on 01/04/20.
//  Copyright © 2020 VBTechlabs. All rights reserved.
//

import UIKit

class HomeHelpTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var viewHelp: UIView!
    
    @IBOutlet weak var btnCancel: CustomeButton!
    @IBOutlet weak var btnSubmit: CustomeButton!
    
    @IBOutlet weak var txtHelpTitle: CustomeTextField!
    
    @IBOutlet weak var txtHelpDetail: CustomeTextView!
    
    @IBOutlet weak var lblDetailPlaceholder: UILabel!
    
    @IBOutlet weak var viewHelpHeight: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        txtHelpTitle.leftPadding = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
